
let _demovideo;
let videoCapture
let img;
function preload() {
    img = loadImage('./img.png');
    _demovideo=createVideo(['./media/abc-99.mp4']);
    _demovideo.hide();
    console.log('ok')
}
async function videoReady() {
    _demovideo.elt.muted = true;
    _demovideo.elt.play();
    _demovideo.loop();
    draw=draw_;    
}
function setup() {
    ccview = createCanvas(windowWidth, windowHeight);
    gui = createGui();
    videoCapture = createCapture(VIDEO, videoReady);
    videoCapture.elt.muted = true;
    videoCapture.hide();
    draw=()=>{}
}
function draw_(){
    if(true){//背景襯底
        let a=windowWidth/img.width;
        let b=windowHeight/img.height;
        let _max=Math.max(a,b);
        push();
        scale(_max, _max);
        image(img, windowWidth/2/_max-img.width/2, windowHeight/2/_max-img.height/2);
        pop();
    }
    if(true){//鏡頭擷取
        let a=windowWidth/videoCapture.width;
        let b=windowHeight/videoCapture.height;
        let _min=Math.min(a,b);
        push();
        scale(-_min, _min);//鏡射
        image(videoCapture, -windowWidth/2/_min-videoCapture.width/2, windowHeight/2/_min-videoCapture.height/2);
        pop();
    }
    if(true){//教練展示
        push();
        scale(0.3, 0.3)
        image(_demovideo, 10/0.3, 10/0.3);
        pop();
    }
    if(true){//畫定位格
        push()
        noFill();
        for (let i=0;i<windowWidth;i+=100){
            for (let j=0;j<windowHeight;j+=100){
                rect(i, j, i+100, j+100);
            }
        }
        pop()
    }
}
function windowResized() {//視窗縮放
    resizeCanvas(windowWidth, windowHeight);
}



