
let _demovideo;
let videoCapture
let img;

let stageStatus = 0;
const stageOb1 = {};

function preload() {
    // img = loadImage('./img.png');
    // _demovideo=createVideo(['./media/abc-99.mp4']);
    // _demovideo.hide();

    stageOb1.bgVideo = createVideo(['./media/stage1_bg.mp4']);
    stageOb1.bgVideo.hide();
    stageOb1.bgbgVideo = createVideo(['./media/stage1_bgbg.mp4'],videoReady); //bg的bg
    stageOb1.bgbgVideo.hide();
    
    console.log('ok')
}
async function videoReady() {
    // _demovideo.elt.muted = true;
    // _demovideo.elt.play();
    // _demovideo.loop();

    // stageOb1.bgVideo.loop();
    stageOb1.bgVideo.volume(0);
    stageOb1.bgbgVideo.volume(0);
    stageOb1.bgVideo.loop();
    stageOb1.bgbgVideo.loop();

    draw=draw_;    
}
function setup() {
    console.log("setup")
    ccview = createCanvas(windowWidth, windowHeight);
    gui = createGui();
    // videoCapture = createCapture(VIDEO, videoReady);
    // videoCapture.elt.muted = true;
    // videoCapture.hide();


    stageOb1.startBtn = createButton("繼 續", windowWidth/2-150, windowHeight*0.88, 300, 60);
    stageOb1.startBtn.setStyle({
        strokeWeight:0,
        rounding:30,
        fillBg:color(0,80,150,230),
        fillBgHover:color(0,80,150,250),
        fillBgActive:color(0,80,150,250),
        textSize: 40,
        fillLabel:color(200,230,255),
        fillLabelHover:color(255,255,255),
        fillLabelActive:color(255,255,255),
    });
    
    

    draw=()=>{}
}
function draw_(){
    // test();
    switch (stageStatus) {
        case 0:
            stage1();
            break;
        case 1:
            stage2();
            break;
        case 2:
            stage3();
            break;
        default:
            break;
    }
    
    return;
    if(true){//背景襯底
        let a=windowWidth/img.width;
        let b=windowHeight/img.height;
        let _max=Math.max(a,b);
        push();
        scale(_max, _max);
        image(img, windowWidth/2/_max-img.width/2, windowHeight/2/_max-img.height/2);
        pop();
    }
    if(true){//鏡頭擷取
        let a=windowWidth/videoCapture.width;
        let b=windowHeight/videoCapture.height;
        let _min=Math.min(a,b);
        push();
        scale(-_min, _min);//鏡射
        image(videoCapture, -windowWidth/2/_min-videoCapture.width/2, windowHeight/2/_min-videoCapture.height/2);
        pop();
    }
    if(true){//教練展示
        push();
        scale(0.3, 0.3)
        image(_demovideo, 10/0.3, 10/0.3);
        pop();
    }
    if(true){//畫定位格
        push()
        noFill();
        for (let i=0;i<windowWidth;i+=100){
            for (let j=0;j<windowHeight;j+=100){
                rect(i, j, i+100, j+100);
            }
        }
        pop()
    }
}
function windowResized() {//視窗縮放
    resizeCanvas(windowWidth, windowHeight);
}

function mousePressed() {

    // stageOb1.bgVideo.loop(); // set the video to loop and start playing
    // stageOb1.bgbgVideo.loop();
}

// ==================

function stage1(){
    clear();

    const videoRatio = stageOb1.bgVideo.width/stageOb1.bgVideo.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;

    image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    image(stageOb1.bgVideo, (windowWidth-windowHeight*videoRatio)/2, 0, windowHeight*videoRatio, windowHeight);
    
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.4,
        panelOffset:30,
        panelY:0.45, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.8)",
        title:"核心肌群組合",
        subtitle:"預計花費時間10分鐘",
        help:"健身新手會建議先以多關節訓練為主。 大肌群訓練大多以「多關節」訓練為主，健身的三巨頭項目 深蹲、硬舉、臥推，一個運動軌跡便會訓練到身體許多部位，同時也耗費較多力氣。 例：深蹲主要會訓練到背+腿部大肌群。",
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    fill("#D5FF31");
    textSize(40);
    text(config.title, panelXY[0], panelXY[1], config.panelWidth);

    fill("#ddd");
    textSize(36);
    text(config.subtitle, panelXY[0], panelXY[1]+50, config.panelWidth, config.panelHeight);

    fill("#ccc");
    textSize(30);
    // textLeading(15);
    textWrap(CHAR);
    // console.log(panelXY[0], config.panelWidth)
    text(config.help, panelXY[0]+10, panelXY[1]+110, config.panelWidth-20, config.panelHeight);

    stageOb1.startBtn.x = windowWidth/2-150;
    stageOb1.startBtn.y = windowHeight*0.88;
    if(stageOb1.startBtn.isPressed){
        // stageOb1.startBtn = createButton("開 始");
        // stageOb1.startBtn.position(windowWidth/2, windowHeight/2);
        // stageOb1.startBtn.mousePressed(e=>{
            console.log("@@")
        // });
        stageStatus = 1;
    }
    drawGui();
}

function stage2(){
    clear();
    // const videoRatio = stageOb1.bgVideo.width/stageOb1.bgVideo.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;
    const windowRatio = windowWidth/windowHeight;

    if(video2Ratio<windowRatio){
        image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    }else{
        image(stageOb1.bgbgVideo, (windowWidth-windowHeight*video2Ratio)/2, 0, windowHeight*video2Ratio, windowHeight);
    }
    
    // image(stageOb1.bgVideo, (windowWidth-windowHeight*videoRatio)/2, 0, windowHeight*videoRatio, windowHeight);
    
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.7,
        panelOffset:30,
        panelY:0.2, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.7)",
        list:[
            "深蹲 × 5",
            "開合跳",
            "伏地挺身",
            "交互蹲跳 × 2"
        ],
        active:0,
        tipTitle:"整體循環2次"
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    config.list.forEach((item,i)=>{
        if(i==config.active){
            fill("#D5FF2F");
            textSize(50);
        }else{
            fill("#fff");
            textSize(40);
        }
        // text(item, (windowWidth-textWidth(item))/2, config.panelHeight+20+(i*50), config.panelWidth);
        text(item, panelXY[0], panelXY[1]+30+i*90, config.panelWidth);
    });

    fill("#ccc");
    textSize(30);
    text(config.tipTitle, panelXY[0], panelXY[5]-30, config.panelWidth);
    
    stageOb1.startBtn.x = windowWidth/2-150;
    stageOb1.startBtn.y = windowHeight*0.88;
    if(stageOb1.startBtn.isPressed){
        console.log("##")
        
        stageStatus = 2;
    }
    drawGui();
}

function stage3(){
    clear();

    videoCapture = createCapture(VIDEO);
    videoCapture.elt.muted = true;
    videoCapture.hide();




    
    
}

function test(){ //雜訊
    clear();
    let img = createImage(100, 100); // same as new p5.Image(100, 100);
    img.loadPixels();
    createCanvas(100, 100);
    background(0);

    // helper for writing color to array
    function writeColor(image, x, y, red, green, blue, alpha) {
    let index = (x + y * width) * 4;
    image.pixels[index] = red;
    image.pixels[index + 1] = green;
    image.pixels[index + 2] = blue;
    image.pixels[index + 3] = alpha;
    }

    let x, y;
    // fill with random colors
    for (y = 0; y < img.height; y++) {
    for (x = 0; x < img.width; x++) {
        let red = random(255);
        let green = random(255);
        let blue = random(255);
        let alpha = 255;
        writeColor(img, x, y, red, green, blue, alpha);
    }
    }

    // draw a red line
    y = 0;
    for (x = 0; x < img.width; x++) {
    writeColor(img, x, y, 255, 0, 0, 255);
    }

    // draw a green line
    y = img.height - 1;
    for (x = 0; x < img.width; x++) {
    writeColor(img, x, y, 0, 255, 0, 255);
    }

    img.updatePixels();
    image(img, 0, 0);
}

