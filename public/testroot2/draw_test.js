
let _demovideo;
let videoCapture
let img;

let stageStatus = 0;
const stageOb1 = {};
const stageOb3 = {};




function preload() {
    // img = loadImage('./img.png');
    // _demovideo=createVideo(['./media/abc-99.mp4']);
    // _demovideo.hide();

    //stage1~2
    stageOb1.bgVideo = createVideo(['./media/stage1_bg.mp4']);
    stageOb1.bgVideo.hide();
    stageOb1.bgbgVideo = createVideo(['./media/stage1_bgbg.mp4'],videoReady); //bg的bg
    stageOb1.bgbgVideo.hide();

    //stage3
    stageOb3.cam = createCapture(VIDEO);
    stageOb3.cam.hide();
    stageOb3.isReady = false;
    stageOb3.isStart = false;
    stageOb3.timeTip = {
        bg:"#000",
        lines:[],
        lineWidth:20,
        lineCount:100,
    };
    // stageOb3.cam.size(80,60);
    // for(let i=0;i<1000;i++){
    //     particles[i] = new Particle(320,240,stageOb3.cam);
    // }
    
    

    console.log('ok')
}
async function videoReady() {
    // _demovideo.elt.muted = true;
    // _demovideo.elt.play();
    // _demovideo.loop();

    // stageOb1.bgVideo.loop();
    stageOb1.bgVideo.volume(0);
    stageOb1.bgbgVideo.volume(0);
    stageOb1.bgVideo.loop();
    stageOb1.bgbgVideo.loop();

    draw=draw_;    
}
function setup() {
    console.log("setup")
    // frameRate(6)
    ccview = createCanvas(windowWidth, windowHeight);
    gui = createGui();
    // videoCapture = createCapture(VIDEO, videoReady);
    // videoCapture.elt.muted = true;
    // videoCapture.hide();
    // console.log(width,height)

    stageOb1.startBtn = createButton("繼 續", windowWidth/2-150, windowHeight*0.88, 300, 60);
    stageOb1.startBtn.setStyle({
        strokeWeight:0,
        rounding:30,
        fillBg:color(0,80,150,230),
        fillBgHover:color(0,80,150,250),
        fillBgActive:color(0,80,150,250),
        textSize: 40,
        fillLabel:color(200,230,255),
        fillLabelHover:color(255,255,255),
        fillLabelActive:color(255,255,255),
    });
    
    for (let i = 0; i < stageOb3.timeTip.lineCount; i++) {
        const x = random(-width);
        const y = random(height*0.3, height*0.7); //start off screen
        const r = random(0.2, 1.2);
        const b = random(10, 250); //opacity
        const s = random(20, 60); //speed
        stageOb3.timeTip.lines.push(new WaterFall(x, y, r, b, s));
        
    }
    

    draw=()=>{}
}
function draw_(){
    // test();
    switch (stageStatus) {
        case 0:
            stage1();
            break;
        case 1:
            stage2();
            break;
        case 2:
            stage3();
            break;
        default:
            break;
    }
    
    return;
    if(true){//背景襯底
        let a=windowWidth/img.width;
        let b=windowHeight/img.height;
        let _max=Math.max(a,b);
        push();
        scale(_max, _max);
        image(img, windowWidth/2/_max-img.width/2, windowHeight/2/_max-img.height/2);
        pop();
    }
    if(true){//鏡頭擷取
        let a=windowWidth/videoCapture.width;
        let b=windowHeight/videoCapture.height;
        let _min=Math.min(a,b);
        push();
        scale(-_min, _min);//鏡射
        image(videoCapture, -windowWidth/2/_min-videoCapture.width/2, windowHeight/2/_min-videoCapture.height/2);
        pop();
    }
    if(true){//教練展示
        push();
        scale(0.3, 0.3)
        image(_demovideo, 10/0.3, 10/0.3);
        pop();
    }
    if(true){//畫定位格
        push()
        noFill();
        for (let i=0;i<windowWidth;i+=100){
            for (let j=0;j<windowHeight;j+=100){
                rect(i, j, i+100, j+100);
            }
        }
        pop()
    }
}
function windowResized() {//視窗縮放
    resizeCanvas(windowWidth, windowHeight);
}

function mousePressed() {
    stageOb3.isReady = !stageOb3.isReady;
    // stageOb1.bgVideo.loop(); // set the video to loop and start playing
    // stageOb1.bgbgVideo.loop();
}

// ==================

function stage1(){
    clear();

    const videoRatio = stageOb1.bgVideo.width/stageOb1.bgVideo.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;

    image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    image(stageOb1.bgVideo, (windowWidth-windowHeight*videoRatio)/2, 0, windowHeight*videoRatio, windowHeight);
    
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.4,
        panelOffset:30,
        panelY:0.45, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.8)",
        title:"核心肌群組合",
        subtitle:"預計花費時間10分鐘",
        help:"健身新手會建議先以多關節訓練為主。 大肌群訓練大多以「多關節」訓練為主，健身的三巨頭項目 深蹲、硬舉、臥推，一個運動軌跡便會訓練到身體許多部位，同時也耗費較多力氣。 例：深蹲主要會訓練到背+腿部大肌群。",
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    fill("#D5FF31");
    textSize(40);
    text(config.title, panelXY[0], panelXY[1], config.panelWidth);

    fill("#ddd");
    textSize(36);
    text(config.subtitle, panelXY[0], panelXY[1]+50, config.panelWidth, config.panelHeight);

    fill("#ccc");
    textSize(30);
    // textLeading(15);
    textWrap(CHAR);
    // console.log(panelXY[0], config.panelWidth)
    text(config.help, panelXY[0]+10, panelXY[1]+110, config.panelWidth-20, config.panelHeight);

    stageOb1.startBtn.x = windowWidth/2-150;
    stageOb1.startBtn.y = windowHeight*0.88;
    if(stageOb1.startBtn.isPressed){
        // stageOb1.startBtn = createButton("開 始");
        // stageOb1.startBtn.position(windowWidth/2, windowHeight/2);
        // stageOb1.startBtn.mousePressed(e=>{
            console.log("@@")
        // });
        stageStatus = 1;
    }
    drawGui();
}

function stage2(){
    clear();
    // const videoRatio = stageOb1.bgVideo.width/stageOb1.bgVideo.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;
    const windowRatio = windowWidth/windowHeight;

    if(video2Ratio<windowRatio){
        image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    }else{
        image(stageOb1.bgbgVideo, (windowWidth-windowHeight*video2Ratio)/2, 0, windowHeight*video2Ratio, windowHeight);
    }
    
    // image(stageOb1.bgVideo, (windowWidth-windowHeight*videoRatio)/2, 0, windowHeight*videoRatio, windowHeight);
    
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.7,
        panelOffset:30,
        panelY:0.2, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.7)",
        list:[
            "深蹲 × 5",
            "開合跳",
            "伏地挺身",
            "交互蹲跳 × 2"
        ],
        active:0,
        tipTitle:"整體循環2次"
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    config.list.forEach((item,i)=>{
        if(i==config.active){
            fill("#D5FF2F");
            textSize(50);
        }else{
            fill("#fff");
            textSize(40);
        }
        // text(item, (windowWidth-textWidth(item))/2, config.panelHeight+20+(i*50), config.panelWidth);
        text(item, panelXY[0], panelXY[1]+30+i*90, config.panelWidth);
    });

    fill("#ccc");
    textSize(30);
    text(config.tipTitle, panelXY[0], panelXY[5]-30, config.panelWidth);
    
    stageOb1.startBtn.x = windowWidth/2-150;
    stageOb1.startBtn.y = windowHeight*0.88;
    if(stageOb1.startBtn.isPressed){
        stageStatus = 2;
    }
    drawGui();
}

function stage3(){
    clear();
    background(40);

    const camRatio = stageOb3.cam.width/stageOb3.cam.height;
    const windowRatio = windowWidth/windowHeight;

    image(stageOb3.cam, (windowWidth-windowHeight*camRatio)/2, 0, windowHeight*camRatio, windowHeight);
    // console.log(stageOb3.cam.width)
    // console.log("frameCount",frameCount,Date.now())

    // stageOb3.cam.loadPixels();
    // for(let i=0;i<particles.length;i++){
    //     particles[i].update();
    //     particles[i].show();
    // }
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.4,
        panelOffset:30,
        panelY:0.3, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.8)",
        panelBG2:"rgba(251,255,79,0.3)",
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    


    if(stageOb3.isReady && !stageOb3.isStart){
        if(!stageOb3.startTime){
            stageOb3.startTime = Date.now();
        }else{
            const readyTime = 3 - ((Date.now() - stageOb3.startTime)/1000>>0);

            
            // strokeWeight(0);
            // fill(config.panelBG);
            // quad(...panelXY);

            

            if(readyTime>0){
                strokeWeight(0);
                fill(config.panelBG);
                quad(...panelXY);
                // var blue = random(0, 60);
                // background(30 - (blue / 3), 30 - (blue / 3), blue, .07);
                strokeWeight(stageOb3.timeTip.lineWidth);

                stageOb3.timeTip.lines.forEach(lob=>{
                    lob.move();
                    lob.display();
                });
                
                strokeWeight(15);
                textStyle(BOLD);
                textAlign(CENTER);
                textSize(150);
                stroke(60, 180, 220);
                fill("#fff");
                text(readyTime, 0, windowHeight/2-75, windowWidth);
            }else if(readyTime>-2){
                strokeWeight(0);
                fill(config.panelBG2);
                quad(...panelXY);

                strokeWeight(15);
                textStyle(BOLD);
                textAlign(CENTER);
                textSize(150);
                stroke(250, 180, 30);
                fill("#ffe");
                text("Start", 0, windowHeight/2-75, windowWidth);
            }else{
                stageOb3.isStart = true;
            }
        }
    }else{
        stageOb3.startTime = null;
    }
    // const ctx = document.querySelector("#defaultCanvas0").getContext("2d");
    // // c.rect(50,50,80,100);

    // ctx.beginPath()
    // //沿用寬度及色彩設定
    // ctx.lineWidth = 3;
    // ctx.strokeStyle = "#adf"
    // ctx.fillStyle = "#0df6"
    // /*使用arc(x,y,r,s,e)畫一個圓
    // x,y是圓心的座標，r是半徑，s和e是起點和終點的角度*/
    // ctx.arc(windowWidth/2, windowHeight/2, 100,0,Math.PI*2)
    // ctx.fill()
    // ctx.stroke()
    
}

function aaa(){

}

function test(){ //雜訊
    clear();
    let img = createImage(100, 100); // same as new p5.Image(100, 100);
    img.loadPixels();
    createCanvas(100, 100);
    background(0);

    // helper for writing color to array
    function writeColor(image, x, y, red, green, blue, alpha) {
    let index = (x + y * width) * 4;
    image.pixels[index] = red;
    image.pixels[index + 1] = green;
    image.pixels[index + 2] = blue;
    image.pixels[index + 3] = alpha;
    }

    let x, y;
    // fill with random colors
    for (y = 0; y < img.height; y++) {
    for (x = 0; x < img.width; x++) {
        let red = random(255);
        let green = random(255);
        let blue = random(255);
        let alpha = 255;
        writeColor(img, x, y, red, green, blue, alpha);
    }
    }

    // draw a red line
    y = 0;
    for (x = 0; x < img.width; x++) {
    writeColor(img, x, y, 255, 0, 0, 255);
    }

    // draw a green line
    y = img.height - 1;
    for (x = 0; x < img.width; x++) {
    writeColor(img, x, y, 0, 255, 0, 255);
    }

    img.updatePixels();
    image(img, 0, 0);
}

function WaterFall(tempX, tempY, tempDiameter, tempB, s) {
    this.x = tempX;
    this.y = tempY;
    this.diameter = tempDiameter;
    this.b = tempB;
    this.s = s;
    this.move = function() {
        // var range = 1;
        // var xspeed = this.s;
        // angle += xspeed;
        // var ty = sin(angle) * range; //create swaying effect along x axis

        var tx = random(1, 5)+this.s;
        // var tx = this.s;
        
        // this.x += (tx);
        // if ((this.x > width+(dropletWidth)) || (this.x < -dropletWidth)) {
        //     this.x = round(random(width / dropletWidth)) * dropletWidth;
        // }
        this.x += (tx);
        if (this.x > width) {
            this.y = random(height*0.35,height*0.65);
            this.x = random(-width);
        }
    }
    this.display = function() {
        stroke(100, 220, 255, 100);
        // fill(0,255,255)
        // line(this.x, this.y, this.x, this.y + this.s * 5000);
        line(this.x, this.y, this.x + this.s*6, this.y);
    }
}