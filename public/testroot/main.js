let pointAry = [//姿態連接
    [4, 2], [2, 0], [0, 1], [1, 3]
    , [10, 8], [8, 6], [6, 5], [5, 7], [7, 9]
    , [5, 11], [11, 13], [13, 15]
    , [6, 12], [12, 14], [14, 16]
    , [12, 11]
]
let ptcov=[0,2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15];//左右交換
let gui;
let cc = 0;
let _stime;
let detector;
let videoCapture
let poses;
let ccview
let frameindex=0;//姿勢目前判斷的frameindex，每完成一個，就下一個
let motionindex=0;//學習活動目前的索引
let motionrepeat=0;//活動腳本目前的重複次數
let itemrepeat=0;//項目重複次數
let jsmotion;//整個學習活動安排

let jsdatactrl;//控制手勢
let fittime=0//吻合後會計數，不吻合會扣分
let errortime=0;//錯誤的次數累積，如果錯誤次數過久，就會播放提示語
let cunt=0//整個動作的完成次數
let ppkey;//原始資料目前對象
TempleteHtml={}
let FitScoreXObj;//判斷套件
// let nowflip____;//目前翻轉狀態
let _demovideo;
let schdurefun;
let voiceary=[];
// let voicestart;
// let voiceready;
let draw2;
let bg_img;
let score_img;
let captureratio;//畫面縮放比
let offsetx;//畫面偏移值
let offsety;//畫面偏移值
let apos//所有位置的物件
// https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready
//https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbDJpVnRJMjF4Z0c5bVJQZjF6SkZNampuVVJDQXxBQ3Jtc0tsMnFTMGVSV2gxaDZpM2wyYS1iSmxnQzZ5VE1FNFZqOXMzQUIzYmttQk5hVVh1QjFaeUMxYWl3WXNIbmpQN3l5TWw4eGZqZjlNVTZtakNxcExvNDJQakFKT0ZMc09rOVVwb1RBVFFLZ1pWcXBMQ2lGQQ&q=https%3A%2F%2Ftranslate.google.com%2Ftranslate_tts%3Fie%3DUTF-8%26client%3Dtw-ob%26tl%3Den%26q%3D/ready
// https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready
// let stream;
let stageStatus = 0;
const stageOb1 = {};
let drawtimer=0;//每次draw時都會+1，可以拿來做動畫時間
let lines;//飛躍的分子系統
let nowsecond;//各個draw要用的時間依據。
let getposes=true;

function preload() {
    soundFormats('mp3', 'ogg');
    // v1mp3=
    stageOb1.bgVideo = createVideo(['./media/stage1_bg.mp4']);
    stageOb1.bgVideo.hide();
    stageOb1.bgbgVideo = createVideo(['./media/stage1_bgbg.mp4']); //bg的bg
    stageOb1.bgbgVideo.hide();

    // voiceready=loadSound('voice_ready.mp3');
    // voicestart=loadSound('voice_start.mp3');
    voiceary.push(loadSound('voice_1.mp3'));
    voiceary.push(loadSound('voice_2.mp3'));
    voiceary.push(loadSound('voice_3.mp3'));
    voiceary.push(loadSound('voice_4.mp3'));
    // v2mp3=loadSound('voice_2.mp3');
    // v3mp3=loadSound('voice_3.mp3');
    // v4mp3=loadSound('voice_4.mp3');
    bg_img = loadImage('./img.png');
    score_img = loadImage('./_score.jpg');
    // _demovideo=createVideo(['./media/abc-99.mp4']);
    // _demovideo.hide();
    console.log('ok')
    // v2mp3=loadSound('https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready')
}
function vidLoad() {
    // _demovideo.src="./media/_dddd2.mp4";
    // _demovideo.elt.multed();
    _demovideo.play();
    _demovideo.loop();
    // console.log('啊')
}
async function videoReady() {
    // console.log('攝影機準備好了')
    // playVoice('攝影機準備好了');
    stageOb1.bgVideo.volume(0);
    stageOb1.bgbgVideo.volume(0);
    stageOb1.bgVideo.loop();
    stageOb1.bgbgVideo.loop();

    $.getJSON("./ctrl_pos.json", async function (__json) {
        // _demovideo.play();
        // _demovideo.loop();

        // _demovideo.hide();
        jsdatactrl = __json;//控制項目先載入
        
        // Set the source of the <video> element to be the stream from the <canvas>.
        // videoCapture.elt.srcObject = stream;
        motionindex=0;
        motionrepeat=0;
        itemrepeat=0;
        
        if(localStorage.getItem('mypreview_demodata')!=null){
            //如果有預覽項目
            // console.log('=======',localStorage.getItem('mypreview_demodata'));
            let jsdata=JSON.parse(localStorage.getItem('mypreview_demodata'));
            localStorage.removeItem('mypreview_demodata')
            if(jsdata.repeat){//從學習Motion預覽
                jsmotion=jsdata;
                
            }else{//從item轉來的preview
                console.log('單一測試')
                jsmotion={//學習活動的框架
                    title:'上身活動套裝',
                    comment:'連續動作注意各種細節。',
                    lv:1,
                    repeat:2,//此活動需要重複幾次
                    items:[
                        {
                            title:jsdata.title,
                            level:1,
                            count:2,
                            holdtime:3,
                            repeat:2,
                            comment:'',
                            settings:[],
                            itemdata:jsdata
                        }
                        
                    ]
                }
    
            }
            console.log(JSON.stringify(jsmotion));
            startrun()
            await getPoses();
        }else{//讀取配置資料檔案
            $.getJSON("data.json", async function (json) {
                console.log(json);
                jsmotion = json;
                startrun();
                await getPoses();
            });
        }
    })
    


    async function startrun(){
        console.log('開始姿勢判讀')
        // playVoice('正在啟動巨匠健身偵測模組','zh-tw');
        playVoice('starting now','en');
        const detectorConfig = {
            modelType: poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING,
        };
        detector = await poseDetection.createDetector(
            poseDetection.SupportedModels.MoveNet,
            detectorConfig
        );
        schdure(draw_allstart,3000,()=>{
            drawtimer=0;
            getposes=false;
            return;
            stageOb1.startBtn = createButton("繼 續", windowWidth/2-150, windowHeight*0.88, 300, 60);
            stageOb1.startBtn.setStyle({
                strokeWeight:0,
                rounding:30,
                fillBg:color(0,80,150,230),
                fillBgHover:color(0,80,150,250),
                fillBgActive:color(0,80,150,250),
                textSize: 40,
                fillLabel:color(200,230,255),
                fillLabelHover:color(255,255,255),
                fillLabelActive:color(255,255,255),
            });        
        })
        // setTimeout(() => {
        //     // draw = draw_nextstart;  
        //     draw = draw_allstart;  
        // }, 3000);
    }
    async function getPoses() {
        if(getposes){
            if (detector) {
                poses = await detector.estimatePoses(videoCapture.elt);
            }
        }
        requestAnimationFrame(getPoses);
    }

}
function setup() {
    // voiceary=[v1mp3,v2mp3,v3mp3,v4mp3];
    
    // v2mp3.play()
    // return;
    schdurefun={};
    schdurefun['nextfun']=()=>{}
    FitScoreXObj=new FitScoreX();
    ccview = createCanvas(windowWidth, windowHeight);
    // stream = ccview.elt.captureStream(30);
    gui = createGui();
    _stime = new Date();
    draw2=()=>{}
    lines=[];
    for (let i = 0; i < 20; i++) {
        //製造分子系統
        const x = random(-width);
        const y = random(height*0.3, height*0.7); //start off screen
        const r = random(0.2, 1.2);
        const b = random(10, 250); //opacity
        const s = random(20, 60); //speed
        lines.push(new WaterFall(x, y, r, b, s));
    }
    // console.log(lines);
    // return;
    setTimeout(()=>{
        videoCapture = createCapture(VIDEO, videoReady);
        videoCapture.hide();
    },1000)
}
function schdure(nextfun,time,excufun){//在時間到達後，進到另一個函數，重複呼叫無效
    
    if(schdurefun['nextfun']==nextfun){
        // console.log('重複了',nextfun)
        return;
    }
    // console.log('沒重複',nextfun)
    schdurefun['nextfun']=nextfun
    setTimeout(()=>{
        draw2=schdurefun['nextfun'];
        schdurefun['nextfun']=()=>{}
        if(excufun){//如果有事件，就執行
            excufun();
        }
    },time);
}
function draw(){
    drawtimer++;
    if(true){//背景襯底
        let a=windowWidth/bg_img.width;
        let b=windowHeight/bg_img.height;
        let _max=Math.max(a,b);
        push();
        scale(_max, _max);
        image(bg_img, windowWidth/2/_max-bg_img.width/2, windowHeight/2/_max-bg_img.height/2);
        pop();
    }
    if(videoCapture){//鏡頭擷取
        let a=windowWidth/videoCapture.width;
        let b=windowHeight/videoCapture.height;
        let _min=Math.min(a,b);
        captureratio=_min;
        offsetx=(windowWidth/2-videoCapture.width*captureratio/2)
        offsety=(windowHeight/2-videoCapture.height*captureratio/2)
        push();
        scale(-_min, _min);//鏡射
        image(videoCapture, -windowWidth/2/_min-videoCapture.width/2, windowHeight/2/_min-videoCapture.height/2);
        pop();
    }
    apos={//各種位置
        width:windowWidth,
        height:windowHeight,
        videowidth:windowWidth-offsetx*2,
        videoheight:windowHeight-offsety*2,
        videocenter:offsetx+windowWidth/2,
        videoleft:offsetx,
        videoright:windowWidth-offsetx,
        videomiddle:offsety+windowHeight/2,
        videotop:offsety,
        videobottom:windowHeight-offsety,
        titleArea:()=>{//標題位置
            let c={
                left:apos.videoleft+100,
                top:apos.videomiddle-200,
                right:apos.videoright-100,
                bottom:apos.videomiddle+50,
            }
            c.width=c.right-c.left;
            c.height=c.bottom-c.top;
            return c;
        }
    }
    // console.log(cvs.titleArea());
    if(false){
        push()
        noFill()
        strokeWeight(1)
        stroke('#ff0000');
        line(apos.videoleft,0,apos.videoleft,windowHeight);
        line(apos.videoright,0,apos.videoright,windowHeight);
        line(0,apos.videotop,windowWidth,apos.videotop);
        line(0,apos.videobottom,windowWidth,apos.videobottom);
        line(0,apos.videomiddle,windowWidth,apos.videomiddle);
        let g=apos.titleArea();
        rect(g.left,g.top,g.width,g.height);
        pop()
    }
    draw2();
}
function draw_allstart(){//完整流程開始
    getposes=false;
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.4,
        panelOffset:30,
        panelY:0.45, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.8)",
        title:"核心肌群組合",
        subtitle:"預計花費時間10分鐘",
        help:"健身新手會建議先以多關節訓練為主。 大肌群訓練大多以「多關節」訓練為主，健身的三巨頭項目 深蹲、硬舉、臥推，一個運動軌跡便會訓練到身體許多部位，同時也耗費較多力氣。 例：深蹲主要會訓練到背+腿部大肌群。",
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    fill("#D5FF31");
    textSize(40);
    text(config.title, panelXY[0], panelXY[1], config.panelWidth);

    fill("#ddd");
    textSize(36);
    text(config.subtitle, panelXY[0], panelXY[1]+50, config.panelWidth, config.panelHeight);

    fill("#ccc");
    textSize(30);
    // textLeading(15);
    textWrap(CHAR);
    // console.log(panelXY[0], config.panelWidth)
    text(config.help, panelXY[0]+10, panelXY[1]+110, config.panelWidth-20, config.panelHeight);
    if(false){
        stageOb1.startBtn.x = windowWidth/2-150;
        stageOb1.startBtn.y = windowHeight*0.88;
        if(stageOb1.startBtn.isPressed){
            // stageOb1.startBtn = createButton("開 始");
            // stageOb1.startBtn.position(windowWidth/2, windowHeight/2);
            // stageOb1.startBtn.mousePressed(e=>{
                console.log("@@")
            // });
            stageStatus = 1;
        }
    }
    drawGui();
    schdure(draw_item_hightlight,3000,()=>{
    
    })
}
function draw_item_hightlight(){//出現項目，以及次數提示
    clear();
    // const videoRatio = stageOb1.bgVideo.width/stageOb1.bgVideo.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;
    const windowRatio = windowWidth/windowHeight;

    if(video2Ratio<windowRatio){
        image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    }else{
        image(stageOb1.bgbgVideo, (windowWidth-windowHeight*video2Ratio)/2, 0, windowHeight*video2Ratio, windowHeight);
    }
    
    // image(stageOb1.bgVideo, (windowWidth-windowHeight*videoRatio)/2, 0, windowHeight*videoRatio, windowHeight);
    let list_=[];
    for (let i=0;i<jsmotion.items.length;i++){
        // console.log('項目名稱',jsmotion.items[i].title);
        // console.log('項目次數',jsmotion.items[i].count);
        // console.log('項目回合',jsmotion.items[i].repeat);
        let repeat=jsmotion.items[i].repeat==1?'':' x ('+(itemrepeat+1)+'/'+jsmotion.items[i].repeat+')'
        list_.push(jsmotion.items[i].title+jsmotion.items[i].count+repeat);
    }
    // console.log('全部重複',jsmotion.repeat);
    // console.log('目前項目',motionindex);

    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.7,
        panelOffset:30,
        panelY:0.2, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.7)",
        list:list_,
        // [
        //     "深蹲 × 5",
        //     "開合跳",
        //     "伏地挺身",
        //     "交互蹲跳 × 2"
        // ],
        active:motionindex,
        tipTitle:"整體循環("+(motionrepeat+1)+'/'+jsmotion.repeat+")次"
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    strokeWeight(0);
    fill(config.panelBG);
    quad(...panelXY);
    
    textStyle(BOLD);
    textAlign(CENTER);
    // textFont("新細明體");

    config.list.forEach((item,i)=>{
        if(i==config.active){
            fill("#D5FF2F");
            textSize(Math.sin(drawtimer/10)*10+45);
            if(jsmotion.items[motionindex].autoflip==1&& itemrepeat>0){
                //除第一次，後續的都會出現請換邊
                item=item+' 請換邊';
            }
        }else{
            fill("#fff");
            textSize(40);
        }
        // text(item, (windowWidth-textWidth(item))/2, config.panelHeight+20+(i*50), config.panelWidth);
        // text(item, panelXY[0], panelXY[1]+30+i*90, config.panelWidth);
        text(item, panelXY[0], panelXY[1]+30+i*90, config.panelWidth);
    });

    fill("#ccc");
    textSize(30);
    // text(config.tipTitle, panelXY[0], panelXY[5]-30, config.panelWidth);
    text(config.tipTitle, panelXY[0], panelXY[5], config.panelWidth);
    if(false){
        stageOb1.startBtn.x = windowWidth/2-150;
        stageOb1.startBtn.y = windowHeight*0.88;
        if(stageOb1.startBtn.isPressed){
            stageStatus = 2;
        }
    }
    drawGui();
    
    schdure(draw_nextstart,3000,()=>{
        nowsecond=Date.now();
        errortime=0;
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.xid+'.mp4'
        //jsdata.demovideo
        console.log('換影片',_demovideopath)
        _demovideo=createVideo([_demovideopath],vidLoad);
        _demovideo.hide();
        // _demovideo.src=_demovideopath;
        _demovideo.loop();
    })

}



function ______draw_allstart(){//完整流程開始
    // flipstatus=jsmotion.items[motionindex].flip;
    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text(jsmotion.title, 320, 240)
        .textAlign(CENTER);

        fill('#000000')
        .strokeWeight(1)
        .textSize(80)
        .stroke('white')
        .text(jsmotion.comment, 320, 400).textAlign(CENTER);
    }
    schdure(draw_nextstart,3000,()=>{
        errortime=0;
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.xid+'.mp4'
        //jsdata.demovideo
        console.log('換影片',_demovideopath)
        _demovideo=createVideo([_demovideopath],vidLoad);
        _demovideo.hide();
        // _demovideo.src=_demovideopath;
        _demovideo.loop();
    })
    
    return;
    setTimeout(()=>{
        draw = draw_nextstart;
    },3000);

}
function flipstatus(){
    // console.log('---',motionindex,itemrepeat,jsmotion);
    let _ll=jsmotion.items[motionindex].flip;
    if(jsmotion.items[motionindex].autoflip==1){
        _ll=(jsmotion.items[motionindex].flip+itemrepeat)%2;
    }
    return _ll;
}
function drawDemovideo(){
    // console.log(flipstatus);
    let _nowflip=flipstatus();
    let _tl=10/0.3
    if(_nowflip==1){//翻轉
        push();
        scale(-0.3, 0.3)
        image(_demovideo, -_demovideo.width-_tl, _tl);
        // image(_demovideo, -_demovideo.width, 0);
        pop();
    }else{
        push();
        scale(0.3, 0.3)
        image(_demovideo, _tl, _tl);
        // image(_demovideo, 10, 10/0.3);
        pop();
    }
}


function draw_nextstart(){
    clear();
    background(40);

    // const camRatio = stageOb3.cam.width/stageOb3.cam.height;
    const video2Ratio = stageOb1.bgbgVideo.width/stageOb1.bgbgVideo.height;
    const windowRatio = windowWidth/windowHeight;

    if(video2Ratio<windowRatio){
        image(stageOb1.bgbgVideo, 0, (windowHeight-windowWidth/video2Ratio)/2, windowWidth, windowWidth/video2Ratio);
    }else{
        image(stageOb1.bgbgVideo, (windowWidth-windowHeight*video2Ratio)/2, 0, windowHeight*video2Ratio, windowHeight);
    }

    // console.log(stageOb3.cam.width)
    // console.log("frameCount",frameCount,Date.now())

    // stageOb3.cam.loadPixels();
    // for(let i=0;i<particles.length;i++){
    //     particles[i].update();
    //     particles[i].show();
    // }
    const config={
        panelWidth:windowWidth*0.7,
        panelHeight:windowHeight*0.4,
        panelOffset:30,
        panelY:0.3, //顯示在Y軸哪個位置
        panelBG:"rgba(0,0,0,0.8)",
        panelBG2:"rgba(251,255,79,0.3)",
    };
    const panelXY = [ //4個角座標
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY-config.panelOffset,
        (windowWidth-config.panelWidth)/2+config.panelWidth,
        windowHeight*config.panelY+config.panelHeight-config.panelOffset,
        (windowWidth-config.panelWidth)/2,
        windowHeight*config.panelY+config.panelHeight+config.panelOffset
    ]
    


    // if(stageOb3.isReady && !stageOb3.isStart){
        // if(!stageOb3.startTime){
            // stageOb3.startTime = Date.now();
        // }else{
            const readyTime = 3 - ((Date.now() - nowsecond)/1000>>0);

            
            // strokeWeight(0);
            // fill(config.panelBG);
            // quad(...panelXY);

            

            if(readyTime>0){
                strokeWeight(0);
                fill(config.panelBG);
                quad(...panelXY);
                // var blue = random(0, 60);
                // background(30 - (blue / 3), 30 - (blue / 3), blue, .07);
                strokeWeight(20);

                lines.forEach(lob=>{
                    lob.move();
                    lob.display();
                });
                
                strokeWeight(15);
                textStyle(BOLD);
                textAlign(CENTER);
                textSize(150);
                stroke(60, 180, 220);
                fill("#fff");
                text(readyTime, 0, windowHeight/2-75, windowWidth);
            }else{
                // if(readyTime>-2){
                strokeWeight(0);
                fill(config.panelBG2);
                quad(...panelXY);

                strokeWeight(15);
                textStyle(BOLD);
                textAlign(CENTER);
                textSize(150);
                stroke(250, 180, 30);
                fill("#ffe");
                text("Start", 0, windowHeight/2-75, windowWidth);
                // playVoice('start','zh-tw')
            //}
            //else{
                schdure(draw_wait,1000,()=>{
                    // voiceready.play();
                    playVoice(jsmotion.items[motionindex].itemdata.settings.voice_ready,'zh-tw');
                    // playVoice('ready','en');
                    schdure(draw_playgo,1000,()=>{
                        getposes=true;
                    })
                })
                // stageOb3.isStart = true;
                // playVoice(jsmotion.items[motionindex].itemdata.settings.voice_ready,'zh-tw');
                // // playVoice('ready','en');
                // schdure(draw_playgo,1000,()=>{})
        
            }
            drawDemovideo()
        // }
    // }else{
    //     stageOb3.startTime = null;
    // }
    // schdure(draw_wait,3000,()=>{
    //     // voiceready.play();
    //     playVoice(jsmotion.items[motionindex].itemdata.settings.voice_ready,'zh-tw');
    //     // playVoice('ready','en');
    //     schdure(draw_playgo,1000,()=>{
            
    //     })
    // })
}
function draw_nextstart_______(){//關卡開始
    // console.log('00000000000=',videoCapture,_demovideo)
    // background("#feeacc")
    //     push();
    // scale(1, 1)
    // image(_demovideo, 100, 200);
    // push();
    // scale(-1, 1)
    // image(videoCapture, -videoCapture.width, 0);
    // pop();
    // push();
    // scale(-1, 1)
    // image(videoCapture, -videoCapture.width, 0);
    // pop();



// return;
    if(false){
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.demovideo;
        if($("#demovideo source").length>0){
            console.log('影片:',$($("#demovideo source")[0]).attr('src'),_demovideopath);
            if($($("#demovideo source")[0]).attr('src')!=_demovideopath){
                $("#demovideo").empty();
                let src=$('<source src="'+_demovideopath+'" type="video/mp4"></source>');
                $("#demovideo").append(src)
                $("#demovideo")[0].load()
                $("#demovideo")[0].play()
                
                // $($("#demovideo source")[0]).attr('src',_demovideopath);
            }
        }else{
            let src=$('<source src="'+_demovideopath+'" type="video/mp4"></source>');
            $("#demovideo").append(src)
            $("#demovideo")[0].load()
            $("#demovideo")[0].play()

            // $("#demovideo").append($('<source src="'+_demovideopath+'" type="video/mp4"></source>'))[0].play();
        }
    }
    // _demovideo.src="./media/_dddd2.mp4";
    // push();
    // scale(0.5, 0.5)
    // image(_demovideo, 100, 200);
    // pop();
// return;
    
    // let _ind=frameindex%jsdata.info.length;
    
    

    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text(jsmotion.items[motionindex].title, 320, 240)
        .textAlign(CENTER);

        fill('#000000')
        .strokeWeight(1)
        .textSize(80)
        .stroke('white')
        .text(jsmotion.items[motionindex].comment, 320, 400).textAlign(CENTER);
    }
    drawDemovideo()
    schdure(draw_wait,3000,()=>{
        // voiceready.play();
        playVoice(jsmotion.items[motionindex].itemdata.settings.voice_ready,'zh-tw');
        // playVoice('ready','en');
        schdure(draw_playgo,1000,()=>{
            
        })
    })
    // setTimeout(()=>{
    //     draw = draw_playgo;
    // },3000);

}
function draw_wait(){

}
function draw_next_turn(){//全部完成，然後重來
    
    // background("#feeacc")
    // if (videoCapture) {
    //     // translate(videoCapture.width, 0);
    //     // scale(-1, 1);
    //     // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();

    // } 
        if(true){
            strokeWeight(15);
            textStyle(BOLD);
            textAlign(CENTER);
            textSize(150);
            stroke(250, 180, 30);
            fill("#ffe");
            text("課 表 循 環 "+(motionrepeat+1), 0, windowHeight/2-75, windowWidth);
        }
    if(false){
    
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('很好！整個課表第2次', 320, 240)
        .textAlign(CENTER);
    }
    
    schdure(draw_item_hightlight,3000,()=>{
    // schdure(draw_nextstart,3000,()=>{
        
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.xid+'.mp4'
        //jsdata.demovideo
        console.log('換影片',_demovideopath)
        _demovideo.src=_demovideopath;
        // "./media/_dddd2.mp4";
        // _demovideo.play();
        _demovideo.loop();
    });
}

function draw_itemclear(){//本項目完成（包含重複次數也滿足）
    if(false){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('很好，下一關', 320, 240)
        .textAlign(CENTER);
    }
    
    
    schdure(draw_item_hightlight,1000,()=>{
    });
}
function draw_itemrepeat(){//item再一次
    // background("#feeacc")
    // if (videoCapture) {
    //     // translate(videoCapture.width, 0);
    //     // scale(-1, 1);
    //     // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();

    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('再一次', 320, 240)
        .textAlign(CENTER);
    }
    let jsdata=jsmotion.items[motionindex].itemdata;
    // let _ind=frameindex%jsdata.info.length;
    // console.log('影片:',jsdata.demovideo);

    if(jsmotion.items[motionindex].autoflip==1){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(100)
        .stroke('white')
        .text('請換邊', 320, 380)
        .textAlign(CENTER);
    }else{

    }
    schdure(draw_nextstart,3000);
    // setTimeout(()=>{
    //     draw = draw_nextstart;
    // },3000);

}
function draw_finish(){//關卡完成
    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        strokeWeight(15);
        textStyle(BOLD);
        textAlign(CENTER);
        textSize(150);
        stroke(250, 180, 30);
        fill("#ffe");
        text("結 束", 0, windowHeight/2-75, windowWidth);

        // fill('#AA660055')
        // .strokeWeight(3)
        // .textSize(140)
        // .stroke('white')
        // .text('結束', 320, 240)
        // .textAlign(CENTER);
    }
    schdure(draw_showscore,3000,()=>{
        drawtimer=0;
    });
    

}
function draw_showscore(){
    getposes=false;
    push()
    let u=Math.min(90,drawtimer);
    u=Math.cos(u*Math.PI/180)*1000+(windowHeight/2-score_img.height/2)
    image(score_img, windowWidth/2-score_img.width/2,u);
    pop()
}
function draw_playgo() {//遊戲進行
    // getposes=true;
    // background("#feeacc")
    if (videoCapture) {
        // push();
        // scale(-1, 1)
        // image(videoCapture, -videoCapture.width, 0);
        // pop();
        // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
        let jsdata;
        let ll;
        if(true){
            if (!poses) {
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(70)
                .stroke('white');
                text('人物請回到畫面中間', 320, 240).textAlign(CENTER);
                return;
            }
            let pose = poses[0]
            if (!pose) {
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(70)
                .stroke('white');
                text('人物請回到畫面中間', 320, 240).textAlign(CENTER);
                return;
            }
            jsdata=jsmotion.items[motionindex].itemdata;
            ppkey=jsdata.frame[frameindex%jsdata.frame.length];
            // for(let i=0;i<30;i++){
            // nowflip=jsmotion.items[motionindex].flip;
            // if(jsmotion.items[motionindex].autoflip==1){
            //     nowflip=(jsmotion.items[motionindex].flip+itemrepeat)%2;
            // }
            // console.log('嘿嘿嘿',nowflip);
            ll=FitScoreXObj.getScore(ppkey/**原始資訊 */,pose/**擷取資訊 */,flipstatus()/**左右互換 */);
            // playVoice('good','en');
            // if(ll==0){
            //     return;
            // }
            // ll=0.8
                // ll=FitScoreX.getScore(ppkey/**原始資訊 */,pose/**擷取資訊 */);
            // }
            // FitScore.getScore(ppkey/**目前輪到的動作對象 */,pose/**攝影機抓到的姿態 */)
            //拿取目前比對分數
            if(false){
                //呈現目前比對成績0~99
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(240)
                .stroke('white');
                text(Math.floor(ll*100), 320, 240).textAlign(CENTER);
            }
            let _ind=frameindex%jsdata.info.length;
            let stayandholdtime=jsdata.info[_ind].stay;
            let voice_notget=jsdata.info[_ind].voice_notget;
            let voice_did=jsdata.info[_ind].voice_did;
            if(!voice_notget){
                voice_notget=''
            }
            if(!voice_did){
                voice_did=''
            }
                
                
            
            let accuracylevel=jsdata.info[_ind].accuracy_level;
            jsdata.info[_ind].demovideo;


            
            if(jsdata.info[_ind].ishold==1){
                stayandholdtime=jsmotion.items[motionindex].holdtime
            }
            if(fittime>1){
                accuracylevel=accuracylevel*.9
                //精度下降，讓持續動作可以容易達標
            }
            if(ll>accuracylevel){//得到正確的精度
                fittime++
                errortime=0;
                if(fittime==1){
                    if(fittime/20<stayandholdtime-1){
                        playVoice('please hold','en');
                    }
                }
                // draw_process(fittime/30,jsdata.info[_ind].stay);
                
                draw_process(fittime/20,stayandholdtime);
                //console.log('9888=',jsdata.info[_ind].ishold,stayandholdtime,jsmotion.items[motionindex].holdtime)
                // if((fittime/30)>=jsdata.info[_ind].stay){
                if((fittime/20)>=stayandholdtime){
                    if(frameindex==0){
                        // voicestart.play();  
                        playVoice('start','en');                      
                    }else{
                        voiceary[_ind%4].play();
                    }
                    
                   
                    // if(jsdata.info[frameindex].ishold==1){
                    //如果這個姿勢是ishold，那麼就必須從設定支撐時間來計算～直到充飽時間才行到下個動作
                    //尚未寫
                    fittime=0;
                    frameindex++;
                    
                    if(jsdata.info[_ind].count>0){
                        drawtimer=0;
                    }
                    cunt+=jsdata.info[_ind].count;
                    if(frameindex%jsdata.info.length==0){//連續動作做完了一次
                        console.log('單一動作完成'+cunt+'次');
                        if(cunt>=jsmotion.items[motionindex].count){
                            console.log('單一動作完成足夠的數量'+cunt);
                                // getposes=false
                            // setTimeout(()=>{
                            //     console.log('休息完畢')
                                // getposes=true;
                                itemrepeat++;
                                // cunt=0;    
                            // },5000)

                            if(itemrepeat>=jsmotion.items[motionindex].repeat){
                                console.log('滿足單一次數與單一重複次數，前進下一關');
                                motionindex++;
                                itemrepeat=0;
                                if(motionindex>=jsmotion.items.length){
                                    console.log('單一活動結束')
                                    motionrepeat++;
                                    motionindex=0;
                                    if(motionrepeat>=jsmotion.repeat){
                                        console.log('全部活動結束')
                                        // playVoice('very well','en');
                                        // motionrepeat=0;
                                        // getposes=false;
                                        // draw2=draw_finish;
                                        getposes=false;
                                        schdure(draw_finish,3000,()=>{
                                            playVoice('very well','en');
                                            motionrepeat=0;
                                            getposes=false;
                                            cunt=0; 
                                        })

                                    }else{
                                        getposes=false;
                                        schdure(draw_next_turn,3000,()=>{
                                            playVoice('then now repeat next turn','en');
                                            console.log('進入第'+motionrepeat+'輪完整課表');
                                            getposes=false;
                                            cunt=0; 
                                            // draw2=draw_next_turn;
                                                //draw_itemclear;
                                        })
    
                                        // playVoice('then now repeat next turn','en');
                                        console.log('進入第'+motionrepeat+'輪完整課表');
                                        // getposes=false;
                                        // draw2=draw_next_turn;
                                    }
                                }else{
                                    //下一關
                                    getposes=false;
                                    schdure(draw_itemclear,3000,()=>{
                                        playVoice('next','en');
                                        getposes=false;
                                        cunt=0; 
                                        // draw2=draw_item_hightlight
                                        //draw_itemclear;
                                    })
    
                                    // playVoice('next','en');
                                    // getposes=false;
                                    // draw2=draw_itemclear;
                                    
                                }
                            }else{
                                // draw2=draw_itemrepeat;
                                getposes=false;
                                schdure(draw_item_hightlight,3000,()=>{
                                    getposes=false;
                                    cunt=0;    
                                    // draw2=draw_item_hightlight;
                                    playVoice('one more time','en');
                                    console.log('單一動作進行重複第'+itemrepeat+'輪');                                        
                                })
                                // getposes=false;
                                // draw2=draw_item_hightlight;
                                // playVoice('one more time','en');
                                // console.log('單一動作進行重複第'+itemrepeat+'輪');
                            }
                        }
                    }
                }
            }else{
                fittime=Math.max(fittime-1,0);
                errortime++;
                // console.log(errortime)
                if(errortime==100 && getposes){
                    // console.log('來這裡',voice_notget)
                    if(voice_notget!=''){
                        playVoice(voice_notget);
                    }
                }
                // draw_process(fittime/30,jsdata.info[_ind].stay);
                // draw_process(fittime/20,stayandholdtime);
            }

            if(true){
                const panelXY = [ //4個角座標
                    20,windowHeight-200,
                    250,windowHeight-240,
                    250,windowHeight-80,
                    20,windowHeight-40
                ]
                push()
                strokeWeight(0);
                fill("rgba(0,0,0,0.7)");
                quad(...panelXY);
                pop();
                push();
                // textStyle(BOLD);
                // textAlign(CENTER);
                
                let u=Math.min(180,drawtimer*5);
                u=Math.cos(u*Math.PI/180)*50+200
                // image(score_img, windowWidth/2-score_img.width/2,u);

                // fill('#AA0000')
                // .strokeWeight(4)
                // .textSize(100)
                strokeWeight(15);
                textStyle(BOLD);
                textAlign(CENTER);
                // textSize(150);
                stroke(250, 180, 30);
                fill("#ffe");

                textSize(u)
                // .stroke('white');    
                let xx=0
                let yy=0
                if(stayandholdtime>0){
                    let gg=(fittime/20)/stayandholdtime;
                    // console.log(gg);
                    let size=20
                    if(gg>0.96){
                        xx=0;yy=0;
                    }else{
                        xx=Math.random()*gg*size-gg*size/2;
                        yy=Math.random()*gg*size-gg*size/2;
                    }
                }
                // console.log(xx,yy);
                text(cunt, 150+xx, (windowHeight-150)+yy).textAlign(CENTER);
                pop()
                push();
                strokeWeight(5);
                textStyle(BOLD);
                textAlign(CENTER);
                // textSize(150);
                stroke(250, 180, 30);
                fill("#ffe");

                textSize(30)
                // .stroke('white');  
                if(getposes){  
                    text('/'+jsmotion.items[motionindex].count, 200, windowHeight-100).textAlign(CENTER);
                }
                pop()

            }
            function draw_process(val,tagval){//呈現累積畫面
                if(tagval==0){
                    return;
                }
                let gg=val/tagval;
                push()
                noStroke()
                fill('#AA6600')
                // .strokeWeight(4)
                // .textSize(40)
                // .stroke('white');    
                // text(Math.floor(gg*100)+'%', 320, 440).textAlign(CENTER);    
                // rect(0,windowHeight-100,gg/windowWidth,windowHeight);
                // rect(0,windowHeight-100,gg/windowWidth,windowHeight);
                // console.log(640*gg);
                rect(0,apos.height-100,windowWidth*gg,100);

                pop()
            }

        }
        if(getposes){
            drawKeypoints(poses);
            drawSkeleton(poses); 
        }
        drawDemovideo();
        // }
        return;

    }

    // drawGui();
    fill('#AA6600')
        .strokeWeight(4)
        .textSize(40)
        .stroke('white');

    text('啟動中', 200, 200).textAlign(CENTER);
}
function drawKeypoints(poses) {//畫肢體點
    let _nowflip=flipstatus()
    if (!poses) {
        return;
    }
    // console.log(poses)
    let pose = poses[0]

    if (!pose) {
        return;
    }
    for (let j = 0; j < pose.keypoints.length; j++) {
        let jj=j
        if(_nowflip==1){
            jj=ptcov[j];
        }
        // console.log(nowflip,'嗯嗯嗯嗯',jj,j)
        let keypoint = pose.keypoints[jj];
        // console.log(keypoint,'pt'+j,ppkey['pt'+j])
        if(!ppkey['pt'+j]){
            continue;
        }else{
            if(ppkey['pt'+j].uselv<-1){
                continue;
            }
        }
        if (keypoint.score > 0.2) {
            fill(100, 255, 0);
            stroke(255, 188, 0);
            ellipse((videoCapture.width-keypoint.x)*captureratio+offsetx, keypoint.y*captureratio+offsety, 10, 10);
        }
    }
}
function drawSkeleton(poses) {//畫肢體線
    let _nowflip=flipstatus()

    if (!poses) {
        return;
    }
    if (!poses[0]) {
        return;
    }
    let pose = poses[0].keypoints
    for (let j = 0; j < pointAry.length; j++) {
        let _a=pointAry[j][0]
        let _b=pointAry[j][1]
        // let jj=j
        if(_nowflip==1){
            _a=ptcov[_a];
            _b=ptcov[_b];
        }

        if(!ppkey['pt'+pointAry[j][0]]){
            continue;
        }
        if(!ppkey['pt'+pointAry[j][1]]){
            continue;
        }

        // else{
        if(ppkey['pt'+pointAry[j][0]].uselv<-1){
            continue;
        }
        if(ppkey['pt'+pointAry[j][1]].uselv<-1){
            continue;
        }
        // }
        let a = pose[_a];
        let b = pose[_b];
        if (a.score < 0.2 || b.score < 0.2) {
            continue;
        }
        strokeWeight(4);
        stroke(255, 180, 0);
        //鏡射的關係，所以要反過來畫
        line(videoCapture.width*captureratio-a.x*captureratio+offsetx, a.y*captureratio+offsety, videoCapture.width*captureratio-b.x*captureratio+offsetx, b.y*captureratio+offsety);
    }
}

function WaterFall(tempX, tempY, tempDiameter, tempB, s) {
    this.x = tempX;
    this.y = tempY;
    this.diameter = tempDiameter;
    this.b = tempB;
    this.s = s;
    this.move = function() {
        // var range = 1;
        // var xspeed = this.s;
        // angle += xspeed;
        // var ty = sin(angle) * range; //create swaying effect along x axis

        var tx = random(1, 5)+this.s;
        // var tx = this.s;
        
        // this.x += (tx);
        // if ((this.x > width+(dropletWidth)) || (this.x < -dropletWidth)) {
        //     this.x = round(random(width / dropletWidth)) * dropletWidth;
        // }
        this.x += (tx);
        if (this.x > width) {
            this.y = random(height*0.35,height*0.65);
            this.x = random(-width);
        }
    }
    this.display = function() {
        stroke(100, 220, 255, 100);
        // fill(0,255,255)
        // line(this.x, this.y, this.x, this.y + this.s * 5000);
        line(this.x, this.y, this.x + this.s*6, this.y);
    }
}

function windowResized() {//視窗縮放
    resizeCanvas(windowWidth, windowHeight);
}
