let pointAry = [//姿態連接
    [4, 2], [2, 0], [0, 1], [1, 3]
    , [10, 8], [8, 6], [6, 5], [5, 7], [7, 9]
    , [5, 11], [11, 13], [13, 15]
    , [6, 12], [12, 14], [14, 16]
    , [12, 11]
]
let MotionData=[//課表，學習活動
    {

    }
]
let gui;
let b;
let cc = 0;
let _stime;
let detector;
let videoCapture
let poses;
let ccview
let frameindex=0;//姿勢目前判斷的frameindex，每完成一個，就下一個
let jsmotion;//整個學習活動安排
let jsdata;//當前的姿勢指向
let jsdatactrl;//控制手勢
let fittime=0//吻合後會計數，不吻合會扣分
let cunt=0//整個動作的完成次數
let fit2;
TempleteHtml={}

async function videoReady() {
    console.log('攝影機準備好了')
    $.getJSON("./ctrl_pos.json", async function (__json) {
        jsdatactrl = __json;//控制項目先載入
        if(localStorage.getItem('mypreview_demodata')!=null){
            //從預覽項目載入
            jsdata=JSON.parse(localStorage.getItem('mypreview_demodata'))
            fit2=new FitData(jsdata);
            localStorage.removeItem('mypreview_demodata')

            jsmotion={//學習活動的框架
                title:'上身活動套裝',
                comment:'連續動作注意各種細節。',
                lv:1,
                items:[
                    {
                        level:1,
                        count:10,
                        holdtime:10,
                        itemdata:jsdata
                    }
                    
                ]
            }



            startrun()
            await getPoses();
        }else{
            //從讀取載入
            $.getJSON("data.json", async function (json) {
                jsdata = json;
                fit2=new FitData(jsdata);
                startrun();
                await getPoses();
            });
        }
    })
    


    async function startrun(){
        console.log('開始姿勢判讀')
        const detectorConfig = {
            modelType: poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING,
        };
        detector = await poseDetection.createDetector(
            poseDetection.SupportedModels.MoveNet,
            detectorConfig
        );
        drawdemoview();
        draw = draw_ready;

    }
    function drawdemoview() {
        let demo = jsdata.demo;
        if(demo.length==0){
            return;
        }
        let demosk = jsdata.demosk;
        let _ff = 0
        let c = $('.canvasview')
        var ctx = c[0].getContext("2d");
        ctx.beginPath();
        ctx.arc(100, 75, 50, 0, 2 * Math.PI);
        ctx.stroke();
        draw();
        function draw() {//不斷繪製展示動畫
            ctx.clearRect(0, 0, 320, 240);
            _ff = (_ff + 1) % demo.length
            let _clo = '#aaaaaa';
            drawKeypoints(demo[_ff], _clo);
            drawSkeleton(demo[_ff], _clo);
            // setTimeout(draw,1000/30)
            requestAnimationFrame(draw);
        }
        function drawKeypoints(_framedata, _clolr) {
            ctx.beginPath();
            for (let i = 0; i < 17; i++) {
                if (demosk[i].uselv < -1) {
                    continue;
                }
                let keypoint = { x: _framedata[i * 2], y: _framedata[i * 2 + 1] }
                ctx.lineWidth = 2;
                ctx.strokeStyle = _clolr;
                ctx.moveTo(keypoint.x, keypoint.y)
                ctx.arc(keypoint.x, keypoint.y, 10, 0, 2 * Math.PI);
                ctx.stroke();
            }
        }
        function drawSkeleton(_framedata, _clolr) {
            ctx.beginPath();
            // if(!poses[0]){
            //     return;
            // }
            // let pose = poses[0].keypoints

            for (let j = 0; j < pointAry.length; j++) {
                let pt1 = pointAry[j][0]
                let pt2 = pointAry[j][1]
                if (demosk[pt1].uselv < -1) {
                    continue;
                }
                if (demosk[pt2].uselv < -1) {
                    continue
                }
                let _pt1 = {
                    x: _framedata[pt1 * 2],
                    y: _framedata[pt1 * 2 + 1]
                }
                let _pt2 = {
                    x: _framedata[pt2 * 2],
                    y: _framedata[pt2 * 2 + 1]
                }
                // let a=pose[pointAry[j][0]];
                // let b=pose[pointAry[j][1]];
                // if (a.score < 0.2 || b.score<0.2) {
                //     continue;
                // }
                ctx.lineWidth = 2;
                ctx.strokeStyle = _clolr;
                ctx.moveTo(_pt1.x, _pt1.y)
                ctx.lineTo(_pt2.x, _pt2.y);
                ctx.stroke();

                // strokeWeight(4);
                // stroke(255,180,0);
                // line(a.x, a.y, b.x, b.y);            
            }
        }


    }


    
    // },3000)
    async function getPoses() {
        if (detector) {
            poses = await detector.estimatePoses(videoCapture.elt);
            // if(poses && recording){
            // projectdata.push(poses);
            // }
        }
        // draw()
        // setTimeout(getPoses, 1000 / 60);
        requestAnimationFrame(getPoses);
    }

}
function setup() {
    if(false){
        $('body').empty()
        let itemview=$('<div>')
        let log=$('<div>')
        $('body').append(itemview)
        $('body').append(log)
        let ary=[];
        for (let i=0;i<5;i++){
            let p=$('<div>ABC'+i+'</div>')
            ary.push(p)
            itemview.append(p)
        }
        let pp=ary[3];
        // itemMoveToZero(pp)
        // itemMoveToUp(pp)
        // itemMoveToUp(pp)
        // itemMoveToDown(pp)
        itemMoveToLast(pp)
        show()
        
        return;
        function show(){
            log.empty()
            for (let i=0;i<ary.length;i++){
                log.append($('<br>'))
                log.append(ary[i].text())
            }
        }
        function itemIndex(item){
            for(let i=0;i<ary.length;i++){
                if(ary[i]==item){
                    return i;
                }
            }
        }
        function itemMoveToZero(item){//調整項目順序到第一個
            let ind=itemIndex(item);
            ary.splice(ind,1);
            ary.unshift(item);
            itemview.prepend(item);
        }
        function itemMoveToLast(item){//調整項目順序到最後
            let ind=itemIndex(item);
            ary.splice(ind,1);
            ary.push(item);
            itemview.append(item);
        }
        function itemMoveToUp(item){//調整項目順序往前
            let ind=itemIndex(item);
            console.log(ind);
            if(ind==0){
                return;
            }
            item.insertBefore(ary[ind-1]); 
            let p=ary.splice(ind,1)[0];
            ary.splice(ind-1,0,p)
        }
        function itemMoveToDown(item){//調整項目順序往後
            let ind=itemIndex(item);
            console.log(ind);
            if(ind>=ary.length-1){
                return;
            }
            item.insertAfter(ary[ind+1]); 
            let p=ary.splice(ind,1)[0];
            ary.splice(ind+1,0,p)

        }
    }
    if(true){
        // http://127.0.0.1:4666/?
        // $('.modal').show()
        // console.log($('.modal')[0])
        // return;
    }
    // if(!localStorage.getItem('mydemodata')){
    //     localStorage.setItem('mydemodata','sdfsdf')
    // }
    

    // return;
    if(false){
        let gg=[1,10,2,20,3,30,4,40,5,50]
        let bb=[1000,10,2000,20,4500,30,4000,40,5000,50];
        console.log(baserange_single(gg))
        console.log(baserange_single(bb))
        function baserange_single(_ary){//將數據統一
            let t=1000000;
            let b=-1000000;
            //紀錄偏移座標的最大最小值。
            let newarya=[]
            for (let i=0;i<_ary.length;i++){
                t=Math.min(t,_ary[i])
                b=Math.max(b,_ary[i])
            }
            for(let i=0;i<_ary.length;i++){
                newarya.push((_ary[i]-t)/(b-t))
            }
            return newarya;
        }
        return
    }
    if(false){
        let gg=[1,10,2,20,3,30,4,40,5,50]
        let bb=[1000,10,2000,20,4500,30,4000,40,5000,50];
        console.log(baserange(gg))
        console.log(baserange(bb))
        console.log(getscore(gg, bb));
        return;
    }
    ccview = createCanvas(640, 480);
    
    if (false) {
        var canvasDiv = document.getElementById('myCanvas');
        var width = canvasDiv.offsetWidth;
        console.log(width)
        var sketchCanvas = createCanvas(width, 450);
        // console.log(sketchCanvas);
        sketchCanvas.parent("myCanvas");
    }
    gui = createGui();
    b = createButton("Button", 50, 150);

    // text(cc,10,10);
    _stime = new Date();
    draw=()=>{}

    b.label = "啟動中..."
    setTimeout(()=>{
        videoCapture = createCapture(VIDEO, videoReady);
        videoCapture.hide();
    
    },1000)
}
function draw_ready() {//預備畫面
    // return;
    // console.log('go');
    background("#feeacc")
    if (videoCapture) {
        // ccview.width=videoCapture.width
        // ccview.height=videoCapture.heigth
        // resizeCanvas(videoCapture.width, videoCapture.heigth);
        image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
        
        // let g=new p5.MediaElement(_video[0])//拿取現有的媒體
        // image(g, 0, 0, 320, 240);
        // console.log(_video);
        // if(!poses){
        // return;
        // }
        // fill(100, 255, 0);
        // noStroke();
        // stroke(255, 188, 0);
        // ellipse(keypoint.position.x, keypoint.position.y, 10, 10);
        // ellipse(Math.random() * 320, Math.random() * 240, 50, 50);
        // console.log(poses);
        drawKeypoints(poses);
        drawSkeleton(poses);
        if(false){
            if (!poses) {
                return;
            }
            // console.log(fit2);
            fit2.start(videoCapture,data,count,hodetime,
            ()=>{//done 1 times

            },
            (wrongstring)=>{//some wrong

            },
            ()=>{//holding...

            }
            );
            fit2.isFit(videoCapture,frameindex,poses[0]);
            let ll=fit2.getResult(frameindex, poses[0]);
            switch(ll){
                case 1://集氣進行
                break;
                case 2://集氣減少
                break;
                case 3://集氣通過
                    frameindex++;
                    console.log(frameindex);
                break;
                case 4://完整動作結束
                break;
                case 5://失去判斷資訊，例如人離開畫面範圍
                break;

            }
            if(true){
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(240)
                .stroke('white');
                // text(Math.floor(ll*100), 320, 240).textAlign(CENTER);
                let ll=fit2.getScore();
                if(ll==-1){
                    return;
                }
                text(Math.floor(ll*100), 320, 240).textAlign(CENTER);
                fill('#AA0000')
                .strokeWeight(4)
                .textSize(100)
                .stroke('white');    
                text(fit2.count(), 80, 440).textAlign(LEFT);

                let val=fit2.fittime()/30;
                let stay=fit2.stay(frameindex)
                if(stay==0){
                    return;
                }
                let gg=val/stay;
                fill('#AA6600')
                .strokeWeight(4)
                .textSize(40)
                .stroke('white');    
                text(Math.floor(gg*100)+'%', 320, 440).textAlign(CENTER);   
            }
            return;    
        }
        
        if(true){
            if (!poses) {
                return;
            }
            // console.log(poses)
            let pose = poses[0]
            if (!pose) {
                return;
            }
            let aa=[];
            //= [176.89861850433076,121.64971413066384,187.17535856938179,112.49216994696977,164.49041586952563,111.17249117719719,198.51129073767086,124.59329758648511,146.54768177020367,124.26120500345714,220.24710068828526,167.05155388296112,119.31435853968912,166.22320039624552,290.3289066622467,130.21865925033296,57.004778758265964,121.00346695687455,233.5807825570484,75.11376828677963,129.93710351586265,72.51046601200534,212.11757192626433,253.3262222611147,132.1273965495553,253.50785688557107,266.07925406923266,200.8056180603055,139.54066645815718,225.13638544459735,210.56491318581269,226.9794653277225,157.85553923758573,210.9831217143097];
            // "pt0": {
            //     "x": 176.59266741989927,
            //     "y": 150.21953578528647,
            //     "weights": 1,
            //     "uselv": -1,
            //     "usex": 1,
            //     "usey": 1
            //     }     
            let bb=[];       
            // aa=[];
            // let p=frameindex%(jsdata.frame.length-1)
            // for(let i=0;i<jsdata.frame.length;i++){
            //     if(jsdata.frame[i].)
            // }
            let ppkey=jsdata.frame[frameindex%jsdata.frame.length];
            // console.log(frameindex,jsdata.frame.length);
            // let ppkey={"pt0": {"x": 176.59266741989927,"y": 150.21953578528647,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt1": {"x": 186.8945574956938,"y": 139.08527461472713,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt2": {"x": 162.11154515954757,"y": 137.6303292041985,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt3": {"x": 200.05040722528855,"y": 152.31553371311273,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt4": {"x": 141.54093323480635,"y": 149.92596681616223,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt5": {"x": 226.73728043954247,"y": 191.20299187966685,"weights": 1,"uselv": 0,"usex": 1,"usey": 1},"pt6": {"x": 117.22932737218423,"y": 195.31932908719384,"weights": 1,"uselv": 0,"usex": 1,"usey": 1},"pt7": {"x": 292.43562347870045,"y": 137.25419381027473,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt8": {"x": 46.991573083724525,"y": 129.93981470849178,"weights": 1,"uselv": -1,"usex": 1,"usey": 1},"pt9": {"x": 223.4381519626354,"y": 86.1932484391739,"weights": 1,"uselv": 0,"usex": 1,"usey": 1},"pt10": {"x": 148.17690774588849,"y": 82.46252257381987,"weights": 1,"uselv": 0,"usex": 1,"usey": 1}}
            // console.log(Object.keys(ppkey));
            if(true){
                let xxx=[];
                let yyy=[];
                let xxxx=[];
                let yyyy=[];
                for (let i in ppkey){
                    let n=i.substr(2,2);
                    let nn=parseInt(n)
                    if(ppkey[i].uselv>-1){
                        if(ppkey[i].usex==1){
                            xxx.push(ppkey[i].x)
                            xxxx.push(pose.keypoints[nn].x)
                        }
                        if(ppkey[i].usey==1){
                            yyy.push(ppkey[i].y)
                            yyyy.push(pose.keypoints[nn].y)
                        }
                    }
                }
                aa=baserange_single(xxx).concat(baserange_single(yyy))
                bb=baserange_single(xxxx).concat(baserange_single(yyyy))
                // console.log(aa)
                // console.log(bb)


            }
            if(false){
                for (let i in ppkey){
                    let n=i.substr(2,2);
                    let nn=parseInt(n)
                    if(ppkey[i].uselv>-1){
                        let xx=ppkey[i].x
                        let yy=ppkey[i].y
                        let bbxx=pose.keypoints[nn].x
                        let bbyy=pose.keypoints[nn].y
                        aa.push(ppkey[i].x,ppkey[i].y)
                        bb.push(pose.keypoints[nn].x,pose.keypoints[nn].y)
                    }
                }
            }
            // console.log("=======")
            // console.log(aa,bb);
            // for (let j = 0; j < pose.keypoints.length; j++) {
            //     let keypoint = pose.keypoints[j];
            //     bb.push(keypoint.x, keypoint.y);
            // }
            // console.log(bb);
            let ll=getscore(aa,bb)
            // console.log(ll);
            if(true){
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(240)
                .stroke('white');
        
                text(Math.floor(ll*100), 320, 240).textAlign(CENTER);
            }
            let _ind=frameindex%jsdata.info.length;
            if(ll>jsdata.info[_ind].accuracy_level){//得到正確的精度
            // if(ll>0.95){//得到確定
                fittime++

                draw_process(fittime/30,jsdata.info[_ind].stay);
                // ＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                if((fittime/30)>=jsdata.info[_ind].stay){
                    // if(jsdata.info[frameindex].ishold==1){
                    //如果這個姿勢是ishold，那麼就必須從設定支撐時間來計算～直到充飽時間才行到下個動作
                    fittime=0;
                    frameindex++;
                    if(frameindex%jsdata.info.length==0){
                        //連續動作做完了一次
                        cunt++
                    }
                }
            }else{
                fittime=Math.max(fittime-1,0);
                draw_process(fittime/30,jsdata.info[_ind].stay);
            }
            fill('#AA0000')
            .strokeWeight(4)
            .textSize(100)
            .stroke('white');    
            text(cunt, 80, 440).textAlign(LEFT);
            function draw_process(val,tagval){//呈現累積畫面
                if(tagval==0){
                    return;
                }
                let gg=val/tagval;
                fill('#AA6600')
                .strokeWeight(4)
                .textSize(40)
                .stroke('white');    
                text(Math.floor(gg*100)+'%', 320, 440).textAlign(CENTER);    
            }

        }
        function drawKeypoints(poses) {
            if (!poses) {
                return;
            }
            // console.log(poses)
            let pose = poses[0]
            if (!pose) {
                return;
            }
            for (let j = 0; j < pose.keypoints.length; j++) {
                let keypoint = pose.keypoints[j];
                if (keypoint.score > 0.2) {
                    fill(100, 255, 0);
                    stroke(255, 188, 0);
                    ellipse(keypoint.x, keypoint.y, 10, 10);
                }
            }
        }
        function drawSkeleton(poses) {
            if (!poses) {
                return;
            }
            if (!poses[0]) {
                return;
            }
            let pose = poses[0].keypoints
            for (let j = 0; j < pointAry.length; j++) {
                let a = pose[pointAry[j][0]];
                let b = pose[pointAry[j][1]];
                if (a.score < 0.2 || b.score < 0.2) {
                    continue;
                }
                strokeWeight(4);
                stroke(255, 180, 0);
                line(a.x, a.y, b.x, b.y);
            }
        }

        // }
        return;

    }

    drawGui();
    fill('#AA6600')
        .strokeWeight(4)
        .textSize(40)
        .stroke('white');

    text('啟動中', 200, 200).textAlign(CENTER);
}
function draw2() {
    clear()
    drawGui();
    cc++;
    text(cc, 10, 100);
}
function draw1() {
    background(200);
    drawGui();

    // fill(0);
    fill('#AA6600')
        .strokeWeight(4)
        .textSize(40)
        .stroke('white');

    // textSize(32);
    // textFont('Georgia');
    // text('Georgia', 12, 30);
    // textFont('Helvetica');
    // text('Helvetica', 12, 60);
    if (false) {
        drawingContext.shadowOffsetX = 5;
        drawingContext.shadowOffsetY = -5;
        drawingContext.shadowBlur = 10;
        drawingContext.shadowColor = 'black';
    }
    let ggg = Math.floor((4000 - (new Date().getTime() - _stime.getTime())) / 1000);
    if (ggg <= 0) {
        //倒數完，就開始進行
        ggg = 0;
        draw = draw2
        b.label = '請站好面對攝影機';
    }
    text(ggg, 10, 100);

    if (b.isPressed) {
        print(b.label + " is pressed.");
    }
}
if (true) {
    let a = [330.86407516365335, 277.8304239401496, 348.86767456359104, 261.1822007481297, 299.29836054395264, 259.038414705424, 360.0142953163965, 287.0935454332918, 254.18631156483792, 284.23682005922694, 373.49487609102243, 404.08256701995015, 229.85897073721947, 354.29156600685786, 180.19315724360973, 166.29843847412718, 172.1752990570449, 319.0564847646509, 324.68399314214463, 153.73353725062344, 316.18217639495015, 157.8579430330424, 184.65892154379677, 519.8658139806734, 196.43382997584166, 461.99306421446386, 174.15023232933294, 176.6895018313591, 172.79311486907733, 175.36205872038656, 111.1533422303616, 500.6742908354115, 107.806423881702, 525.1358907418953]
    // [10,20,30,40,50];
    let b = [330.5159464619701, 278.27043718827935, 350.4493648690773, 261.03535107543644, 299.730142417394, 259.3584641910848, 360.9356248051746, 290.6692497272444, 254.29512157107234, 284.6539413185786, 364.5473474516833, 405.15868531795513, 234.23976679395264, 372.47184772443893, 411.4574403834165, 468.6331534445137, 170.70682668329178, 324.6587632481297, 312.53141560162095, 154.98945507325436, 311.831651340399, 161.16544576059852, 202.06395632013718, 523.7272054239402, 196.12607884585412, 458.8779028990025, 173.63718389572944, 176.5637907185162, 173.5187300303928, 174.9464960645262, 111.4878087983167, 501.10782613778053, 107.70806138949501, 524.9721886689526]
    // console.log(getscore(a, b));
}
function getscore(a, b) {
    let s = 0;
    // console.log(a,getNormalized(a))
    // console.log(b,getNormalized(b))
    // a = getNormalized(a)
    // b = getNormalized(b)
    // a=baserange(a);
    // b=baserange(b);
    for (let i = 0; i < a.length; i++) {
        s += Math.abs(a[i] - b[i]);
    }
    return 1 - (s / a.length)
}
function baserange_single(_ary){//將數據統一
    let t=1000000;
    let b=-1000000;
    //紀錄偏移座標的最大最小值。
    let newarya=[]
    for (let i=0;i<_ary.length;i++){
        t=Math.min(t,_ary[i])
        b=Math.max(b,_ary[i])
    }
    for(let i=0;i<_ary.length;i++){
        newarya.push((_ary[i]-t)/(b-t))
    }
    return newarya;
}
function baser_______________ange(_ary){//將數據統一，扣掉偏移座標，形成兩種不同的型式的座標也能比對，數據必須成雙成對x,y,x,y,x,y...
    let l=100000;
    let t=1000000;
    let r=-100000;
    let b=-1000000;
    //紀錄偏移座標的最大最小值。
    let newarya=[]
    // let newaryb=[]
    for (let i=0;i<_ary.length;i+=2){
        r=Math.max(r,_ary[i])
        l=Math.min(l,_ary[i])
        // console.log(r,l);
    }
    for (let i=1;i<_ary.length;i+=2){
        b=Math.max(b,_ary[i])
        t=Math.min(t,_ary[i])
    }
    // console.log(l,t,r,b);
    for(let i=0;i<_ary.length;i++){
        if(i%2==0){
            newarya.push((_ary[i]-l)/(r-l))
        }else{
            newarya.push((_ary[i]-t)/(b-t))    
        }
    }
    return newarya;
}
function getNor______malized(numbers) {//取得標準化
    let ratio = Math.max.apply(Math, numbers) / 1
    let l = numbers.length
    for (let i = 0; i < l; i++) {
        numbers[i] = numbers[i] / ratio;
    }
    return numbers
}

