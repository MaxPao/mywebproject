let btns;
let btns2;
let ccview;
// let btns3;//紀錄角度與距離
let allbtns;
let pointAry = [//姿態連接
    [4, 2], [2, 0], [0, 1], [1, 3]
    , [10, 8], [8, 6], [6, 5], [5, 7], [7, 9]
    , [5, 11], [11, 13], [13, 15]
    , [6, 12], [12, 14], [14, 16]
    , [12, 11]
]
let _sk=[[250,65, true,1],[285,43,true,0.0],[215,43,true,0.0],[320,65,true,0.0],[180,65,true,0.0],[350,145,true,0.0],[150,145,true,0.0],[420,205,true,0.0],[80,205,true,0.0],[460,150,true,0.0],[40,150,true,0.0],[320,304,true,0.0],[180,304,true,0.0],[355,395,true,0.0],[155,395,true,0.0],[320,500,true,0.0],[190,500,true,0.0]];
//權重是全部加起來得平均後，再得出權重
function setup() {
    // console.log(_innerAngle(-10,10)==20)
    // console.log(_innerAngle(350,10)==20)
    // console.log(_innerAngle(-180,-170)==10)
    // console.log(_innerAngle(-170,-10)==160)
    // console.log(_innerAngle(-170,10)==180)
    // console.log(_innerAngle(-200,90)==70)
    // console.log(_innerAngle(90,-200)==70)
    // console.log(_innerAngle(90,20)==70)
    // draw=()=>{}
    // return;
    ccview = createCanvas(1040, 600);
    createGui();
    btns=[];
    btns2=[];
    // btns3={}
    allbtns=[];
    for(let i=0;i<_sk.length;i++){
        let p=createButton(i,_sk[i][0], _sk[i][1],30,30);
        p.weight=_sk[i][3]
        p.uselv=1;
        p.usex=1;
        p.usey=1;
        btns.push(p)
        // console.log(p.weight)
    }
    for(let i=0;i<_sk.length;i++){
        let p=createButton(i,_sk[i][0]+500, _sk[i][1],30,30);
        p.setStyle({
            fillBg: color("#FFFF00"),
            rounding: 10,
            textSize: 12
        });
        btns2.push(p)
    }
    allbtns=btns.concat(btns2)
}
function draw(){
    background("#feeeff")
    // fill(100, 255, 0);
    // stroke(255, 188, 0);
    // ellipse(320,240, 10, 10);
    // let b = createButton('click me',50,50);
    for(let i=0;i<allbtns.length;i++){
        let b=allbtns[i];
        if(b.isPressed) {
            // b.x=0;b.y=0;
            print(b.label + " is pressed.");
            // drawbase();
            // drawbase()
            // drawSkeleton(getbase(btns),0,1);
        }
        if (b.isHeld) {
            b.x=mouseX-15;
            b.y=mouseY-15;
        }
    }
    strokeWeight(4);
    stroke(255, 180, 0);
    // line(10,10,200,400);
    drawSkeleton(btns);
    drawSkeleton(btns2);
    drawbase();
    // drawdiff();
    drawGui();
    // gogogo()
    function changeBG() {
        let val = random(255);
        background(val);
    }

}

function gogogo(){
    var canvas, context, video, xStart, yStart, xEnd, yEnd;

canvas = document.getElementById("ccview");
context = canvas.getContext("2d");
// canvas.addEventListener("mousedown", mouseDown);
// canvas.addEventListener("mouseup", mouseUp);

// function mouseDown(e) {
//     if (video) {
//         video.pause();
//         video = null;
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(video, xStart, yStart, xEnd - xStart, yEnd - yStart);
    // }
//     xStart = e.offsetX;
//     yStart = e.offsetY;
// }

// function mouseUp(e) {
//     xEnd = e.offsetX;
//     yEnd = e.offsetY;
//     if (xStart != xEnd && yStart != yEnd) {
        // video = document.createElement("video");
        // video.src = "http://techslides.com/demos/sample-videos/small.mp4";
        // video.addEventListener('loadeddata', function() {
        //     console.log("loadeddata");
        //     video.play();
        //     setTimeout(videoLoop, 1000 / 30);
        // });
    // }
// }

// function videoLoop() {
//     if (video && !video.paused && !video.ended) {
//         context.drawImage(video, xStart, yStart, xEnd - xStart, yEnd - yStart);
//         setTimeout(videoLoop, 1000 / 30);
// }
}


function drawSkeleton(_btns,offsetX=0,scale=1){
    for (let j=0;j<pointAry.length;j++){
        strokeWeight(4);
        let a=_btns[pointAry[j][0]];
        let b=_btns[pointAry[j][1]];
        stroke(255,180,0);
        line(a.x*scale+offsetX, a.y*scale, b.x*scale+offsetX, b.y*scale);            
    }
}
// function drawdiff(){
//     // console.log(btns3)
//     for(let i=0;i<btns3.length;i++){
//         // let p=createButton(i,_sk[i][0]+500, _sk[i][1],30,30);
//         // p.setStyle({
//         //     fillBg: color("#FFFF00"),
//         //     rounding: 10,
//         //     textSize: 12
//         // });
//         // btns2.push(p)
//     }
// }
function drawbase(){
    let xyAry1=getbase(btns);
    let xyAry2=getbase(btns2);
    
    let ary1=getary(xyAry1);
    let ary2=getary(xyAry2);
    let score=getscore(btns,btns2);//舊的評估系統
    let scoreX=getscoreX(xyAry1, xyAry2);//新的評估系統
    drawSkeleton(xyAry1,0,100);
    drawSkeleton(xyAry2,120,100);

    fill('#AA6600')
    .strokeWeight(1)
    .textSize(40)
    .stroke('white');
    text(
        (Math.floor(score*100)/100)+
        '/'+(Math.floor(scoreX*100)/100)
        , 250, 50
    ).textAlign(LEFT);
    function getscore(btns, btns2) {
        let xyAry1=getbase(btns);
        let xyAry2=getbase(btns2);
        let a=getary(xyAry1);
        let b=getary(xyAry2);    
 
        let s = 0;
        for (let i = 0; i < a.length; i++) {
            // let _weight=btns[Math.floor(i/2)].weight;
            // console.log('---',btns[Math.floor(i/2)].weight);
            s += Math.abs(a[i] - b[i])
            // *Math.abs(a[i] - b[i])
            // *_weight;
        }
        return 1 - (s / a.length)
    }
    function getary(xyAry){
        let a=[]
        for (let i=0;i<xyAry.length;i++){
            a.push(xyAry[i].x)
            a.push(xyAry[i].y)
        }
        return a;
    }
    function getbase(_btnary){//陣列組合，變成基礎值
        let ax=[]
        let ay=[]
        for(let i=0;i<_btnary.length;i++){
            ax.push(_btnary[i].x);
            ay.push(_btnary[i].y);
        }
        ax=baserange_single(ax);
        ay=baserange_single(ay);
        let cc=[]
        for(let i=0;i<ax.length;i++){
            cc.push({x:ax[i],y:ay[i]})
        }
        // console.log(cc)
        return cc;
    }
    function getscoreX(a, b) {
        let t1=getdiff(a);//紀錄所有的兩點數據，包含距離、角度。
        let t2=getdiff(b);//紀錄所有的兩點數據，包含距離、角度。
        // btns3=[]
        // console.log('==========',t1);
        let addA=0;
        let addB=0;
        for (i in t1){
            let _g=_innerAngle(t1[i].angle,t2[i].angle)
            // Math.abs(t1[i].angle-t2[i].angle)
            _g=_g*_g;
            addA+=_g==0?0:_g/180;
            addB+=Math.abs(t1[i].distance-t2[i].distance)
            // btns3.push({
            //     angle:_innerAngle(t1[i].angle,t2[i].angle),
            //     //Math.abs(t1[i].angle-t2[i].angle),
            //     // distance:Math.abs(t1[i].distance-t2[i].distance),
            //     a:parseInt(i.split('_')[0]),//找到按鈕代號
            //     b:parseInt(i.split('_')[1])//找到按鈕代號
            // });
        }
        addA=1-(addA/Object.keys(t1).length);
        addB=1-(addB/Object.keys(t1).length);
        // console.log(t1['0_1'].angle,t2['0_1'].angle)
        // console.log(addA,addB)
        return Math.max(0,(addA+addB)/2)
        function getdiff(_ary){
            let __t1={};
            for(let i=0;i<_ary.length;i++){
                for (let j=0;j<_ary.length;j++){
                    if(i==j){
                        continue;
                    }
                    let _id=Math.min(i,j)+'_'+Math.max(i,j);
                    //得到3_5這樣的名字
                    __t1[_id]={
                        distance:_dis(_ary[j],_ary[i]),
                        angle:_ang(_ary[j],_ary[i])
                    }
                }
            }  
            return __t1;
        }
    }
}
function _innerAngle(a,b){//兩角度的差異值，以<180的為主。
    let c;
    // a=Math.abs(a);
    // b=Math.abs(b);
    if(a<0){
        a=a+360;
    }
    if(b<0){
        b=b+360;
    }

    if((a>=180 && b<180) || (b>=180 && a<180)){
        let _a=Math.max(a,b);
        let _b=Math.min(a,b);
        let _a1=(_b+360)-_a
        let _b1=_a-_b;
        c=Math.abs(Math.min(_a1,_b1));
    }else{
        c=Math.abs(a-b);
    }
    // console.log(a,b,'=',c);
    return c;
}
function _ang(pot1,pot2){//兩點角度
    let a=Math.atan2(pot2.y-pot1.y,pot2.x-pot1.x)*180/Math.PI;
    // console.log(pot1,pot2,'=ang=',a);
    return a
}
function _dis(pot1,pot2){//兩點距離
    let a=Math.sqrt(Math.pow(pot2.y-pot1.y,2)+Math.pow(pot2.x-pot1.x,2));
    // console.log(pot1,pot2,'=dis=',a);
    return a
}
function baserange_single(_ary){//將數據統一
    let t=1000000;
    let b=-1000000;
    //紀錄偏移座標的最大最小值。
    let newarya=[]
    for (let i=0;i<_ary.length;i++){
        t=Math.min(t,_ary[i])
        b=Math.max(b,_ary[i])
    }
    for(let i=0;i<_ary.length;i++){
        newarya.push((_ary[i]-t)/(b-t))
    }
    return newarya;
}



