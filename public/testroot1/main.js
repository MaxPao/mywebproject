let pointAry = [//姿態連接
    [4, 2], [2, 0], [0, 1], [1, 3]
    , [10, 8], [8, 6], [6, 5], [5, 7], [7, 9]
    , [5, 11], [11, 13], [13, 15]
    , [6, 12], [12, 14], [14, 16]
    , [12, 11]
]
let ptcov=[0,2,1,4,3,6,5,8,7,10,9,12,11,14,13,16,15];//左右交換
let gui;
let cc = 0;
let _stime;
let detector;
let videoCapture
let poses;
let ccview
let frameindex=0;//姿勢目前判斷的frameindex，每完成一個，就下一個
let motionindex=0;//學習活動目前的索引
let motionrepeat=0;//活動腳本目前的重複次數
let itemrepeat=0;//項目重複次數
let jsmotion;//整個學習活動安排

let jsdatactrl;//控制手勢
let fittime=0//吻合後會計數，不吻合會扣分
let cunt=0//整個動作的完成次數
let ppkey;//原始資料目前對象
TempleteHtml={}
let FitScoreXObj;//判斷套件
// let nowflip____;//目前翻轉狀態
let _demovideo;
let schdurefun;
let voiceary=[];
let voicestart;
let voiceready;
let draw2;
let bg_img;
let captureratio;//畫面縮放比
let offsetx;//畫面偏移值
let offsety;//畫面偏移值
let apos//所有位置的物件
// https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready
//https://www.youtube.com/redirect?event=video_description&redir_token=QUFFLUhqbDJpVnRJMjF4Z0c5bVJQZjF6SkZNampuVVJDQXxBQ3Jtc0tsMnFTMGVSV2gxaDZpM2wyYS1iSmxnQzZ5VE1FNFZqOXMzQUIzYmttQk5hVVh1QjFaeUMxYWl3WXNIbmpQN3l5TWw4eGZqZjlNVTZtakNxcExvNDJQakFKT0ZMc09rOVVwb1RBVFFLZ1pWcXBMQ2lGQQ&q=https%3A%2F%2Ftranslate.google.com%2Ftranslate_tts%3Fie%3DUTF-8%26client%3Dtw-ob%26tl%3Den%26q%3D/ready
// https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready
// let stream;
  
function preload() {
    soundFormats('mp3', 'ogg');
    // v1mp3=
    
    voiceready=loadSound('voice_ready.mp3');
    voicestart=loadSound('voice_start.mp3');
    voiceary.push(loadSound('voice_1.mp3'));
    voiceary.push(loadSound('voice_2.mp3'));
    voiceary.push(loadSound('voice_3.mp3'));
    voiceary.push(loadSound('voice_4.mp3'));
    // v2mp3=loadSound('voice_2.mp3');
    // v3mp3=loadSound('voice_3.mp3');
    // v4mp3=loadSound('voice_4.mp3');
    bg_img = loadImage('./img.png');
    // _demovideo=createVideo(['./media/abc-99.mp4']);
    // _demovideo.hide();
    console.log('ok')
    // v2mp3=loadSound('https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q=%2F/ready')
}
function vidLoad() {
    // _demovideo.src="./media/_dddd2.mp4";
    // _demovideo.elt.multed();
    _demovideo.play();
    _demovideo.loop();
    console.log('啊')
}
async function videoReady() {
    // responsiveVoice.speak("攝影機準備好了hello world");
    console.log('--攝影機準備好了')
    $.getJSON("./ctrl_pos.json", async function (__json) {
        // _demovideo.play();
        // _demovideo.loop();

        // _demovideo.hide();
        jsdatactrl = __json;//控制項目先載入
        
        // Set the source of the <video> element to be the stream from the <canvas>.
        // videoCapture.elt.srcObject = stream;
        motionindex=0;
        motionrepeat=0;
        itemrepeat=0;
        
        if(localStorage.getItem('mypreview_demodata')!=null){
            //如果有預覽項目
            // console.log('=======',localStorage.getItem('mypreview_demodata'));
            let jsdata=JSON.parse(localStorage.getItem('mypreview_demodata'));
            localStorage.removeItem('mypreview_demodata')
            if(jsdata.repeat){//從學習Motion預覽
                jsmotion=jsdata;
                
            }else{//從item轉來的preview
                console.log('單一測試')
                jsmotion={//學習活動的框架
                    title:'上身活動套裝',
                    comment:'連續動作注意各種細節。',
                    lv:1,
                    repeat:2,//此活動需要重複幾次
                    items:[
                        {
                            title:jsdata.title,
                            level:1,
                            count:2,
                            holdtime:3,
                            repeat:2,
                            comment:'',
                            settings:[],
                            itemdata:jsdata
                        }
                        
                    ]
                }
    
            }
            console.log(JSON.stringify(jsmotion));
            startrun()
            await getPoses();
        }else{//讀取配置資料檔案
            $.getJSON("data.json", async function (json) {
                console.log(json);
                jsmotion = json;
                startrun();
                await getPoses();
            });
        }
    })
    


    async function startrun(){
        console.log('開始姿勢判讀')
        const detectorConfig = {
            modelType: poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING,
        };
        detector = await poseDetection.createDetector(
            poseDetection.SupportedModels.MoveNet,
            detectorConfig
        );
        schdure(draw_allstart,3000)
        // setTimeout(() => {
        //     // draw = draw_nextstart;  
        //     draw = draw_allstart;  
        // }, 3000);
    }
    async function getPoses() {
        if (detector) {
            poses = await detector.estimatePoses(videoCapture.elt);
        }
        requestAnimationFrame(getPoses);
    }

}
function setup() {
    // voiceary=[v1mp3,v2mp3,v3mp3,v4mp3];
    
    // v2mp3.play()
    // return;
    schdurefun={};
    schdurefun['nextfun']=()=>{}
    FitScoreXObj=new FitScoreX();
    ccview = createCanvas(windowWidth, windowHeight);
    // stream = ccview.elt.captureStream(30);
    gui = createGui();
    _stime = new Date();
    draw2=()=>{}
    setTimeout(()=>{
        videoCapture = createCapture(VIDEO, videoReady);
        videoCapture.hide();
    },1000)
}
function schdure(nextfun,time,excufun){//在時間到達後，進到另一個函數，重複呼叫無效
    
    if(schdurefun['nextfun']==nextfun){
        // console.log('重複了',nextfun)
        return;
    }
    // console.log('沒重複',nextfun)
    schdurefun['nextfun']=nextfun
    setTimeout(()=>{
        draw2=schdurefun['nextfun'];
        schdurefun['nextfun']=()=>{}
        if(excufun){//如果有事件，就執行
            excufun();
        }
    },time);
}
function draw(){
    if(true){//背景襯底
        let a=windowWidth/bg_img.width;
        let b=windowHeight/bg_img.height;
        let _max=Math.max(a,b);
        push();
        scale(_max, _max);
        image(bg_img, windowWidth/2/_max-bg_img.width/2, windowHeight/2/_max-bg_img.height/2);
        pop();
    }
    if(videoCapture){//鏡頭擷取
        let a=windowWidth/videoCapture.width;
        let b=windowHeight/videoCapture.height;
        let _min=Math.min(a,b);
        captureratio=_min;
        offsetx=(windowWidth/2-videoCapture.width*captureratio/2)
        offsety=(windowHeight/2-videoCapture.height*captureratio/2)
        push();
        scale(-_min, _min);//鏡射
        image(videoCapture, -windowWidth/2/_min-videoCapture.width/2, windowHeight/2/_min-videoCapture.height/2);
        pop();
    }
    apos={//各種位置
        width:windowWidth,
        height:windowHeight,
        videowidth:windowWidth-offsetx*2,
        videoheight:windowHeight-offsety*2,
        videocenter:offsetx+windowWidth/2,
        videoleft:offsetx,
        videoright:windowWidth-offsetx,
        videomiddle:offsety+windowHeight/2,
        videotop:offsety,
        videobottom:windowHeight-offsety,
        titleArea:()=>{//標題位置
            let c={
                left:apos.videoleft+100,
                top:apos.videomiddle-200,
                right:apos.videoright-100,
                bottom:apos.videomiddle+50,
            }
            c.width=c.right-c.left;
            c.height=c.bottom-c.top;
            return c;
        }
    }
    // console.log(cvs.titleArea());
    if(false){
        push()
        noFill()
        strokeWeight(1)
        stroke('#ff0000');
        line(apos.videoleft,0,apos.videoleft,windowHeight);
        line(apos.videoright,0,apos.videoright,windowHeight);
        line(0,apos.videotop,windowWidth,apos.videotop);
        line(0,apos.videobottom,windowWidth,apos.videobottom);
        line(0,apos.videomiddle,windowWidth,apos.videomiddle);
        let g=apos.titleArea();
        rect(g.left,g.top,g.width,g.height);
        pop()
    }
    draw2();
}
function draw_allstart(){//完整流程開始
    // flipstatus=jsmotion.items[motionindex].flip;
    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text(jsmotion.title, 320, 240)
        .textAlign(CENTER);

        fill('#000000')
        .strokeWeight(1)
        .textSize(80)
        .stroke('white')
        .text(jsmotion.comment, 320, 400).textAlign(CENTER);
    }
    schdure(draw_nextstart,3000,()=>{
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.xid+'.mp4'
        //jsdata.demovideo
        console.log('換影片',_demovideopath)
        _demovideo=createVideo([_demovideopath],vidLoad);
        _demovideo.hide();
        // _demovideo.src=_demovideopath;
        _demovideo.loop();
    })
    return;
    setTimeout(()=>{
        draw = draw_nextstart;
    },3000);

}
function flipstatus(){
    // console.log('---',motionindex,itemrepeat,jsmotion);
    let _ll=jsmotion.items[motionindex].flip;
    if(jsmotion.items[motionindex].autoflip==1){
        _ll=(jsmotion.items[motionindex].flip+itemrepeat)%2;
    }
    return _ll;
}
function drawDemovideo(){
    // console.log(flipstatus);
    let _nowflip=flipstatus();
    if(_nowflip==1){//翻轉
        push();
        scale(-0.3, 0.3)
        image(_demovideo, -_demovideo.width-10, 20/0.3);
        pop();
    }else{
        push();
        scale(0.3, 0.3)
        image(_demovideo, 10, 20/0.3);
        pop();
    }
}
function draw_nextstart(){//關卡開始
    // console.log('00000000000=',videoCapture,_demovideo)
    // background("#feeacc")
    //     push();
    // scale(1, 1)
    // image(_demovideo, 100, 200);
    // push();
    // scale(-1, 1)
    // image(videoCapture, -videoCapture.width, 0);
    // pop();
    // push();
    // scale(-1, 1)
    // image(videoCapture, -videoCapture.width, 0);
    // pop();



// return;
    if(false){
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.demovideo;
        if($("#demovideo source").length>0){
            console.log('影片:',$($("#demovideo source")[0]).attr('src'),_demovideopath);
            if($($("#demovideo source")[0]).attr('src')!=_demovideopath){
                $("#demovideo").empty();
                let src=$('<source src="'+_demovideopath+'" type="video/mp4"></source>');
                $("#demovideo").append(src)
                $("#demovideo")[0].load()
                $("#demovideo")[0].play()
                
                // $($("#demovideo source")[0]).attr('src',_demovideopath);
            }
        }else{
            let src=$('<source src="'+_demovideopath+'" type="video/mp4"></source>');
            $("#demovideo").append(src)
            $("#demovideo")[0].load()
            $("#demovideo")[0].play()

            // $("#demovideo").append($('<source src="'+_demovideopath+'" type="video/mp4"></source>'))[0].play();
        }
    }
    // _demovideo.src="./media/_dddd2.mp4";
    // push();
    // scale(0.5, 0.5)
    // image(_demovideo, 100, 200);
    // pop();
// return;
    
    // let _ind=frameindex%jsdata.info.length;
    
    

    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text(jsmotion.items[motionindex].title, 320, 240)
        .textAlign(CENTER);

        fill('#000000')
        .strokeWeight(1)
        .textSize(80)
        .stroke('white')
        .text(jsmotion.items[motionindex].comment, 320, 400).textAlign(CENTER);
    }
    drawDemovideo()
    schdure(draw_wait,3000,()=>{
        voiceready.play();
        schdure(draw_playgo,1000,()=>{
            
        })
    })
    // setTimeout(()=>{
    //     draw = draw_playgo;
    // },3000);

}
function draw_wait(){

}
function draw_clear(){//小關卡完成
    // background("#feeacc")
    // if (videoCapture) {
    //     // translate(videoCapture.width, 0);
    //     // scale(-1, 1);
    //     // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();

    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('很好！下一關', 320, 240)
        .textAlign(CENTER);
    }
    schdure(draw_nextstart,3000,()=>{
        
        let jsdata=jsmotion.items[motionindex].itemdata;
        let _demovideopath="./media/"+jsdata.xid+'.mp4'
        //jsdata.demovideo
        console.log('換影片',_demovideopath)
        _demovideo.src=_demovideopath;
        // "./media/_dddd2.mp4";
        // _demovideo.play();
        _demovideo.loop();
    });
return;    
    setTimeout(()=>{
        // _demovideo=createVideo(['./media/_dddd1.mp4'],vidLoad);
        // _demovideo.hide();
        _demovideo.src="./media/_dddd2.mp4";
        _demovideo.play();
        _demovideo.loop();
        draw = draw_nextstart;
    },3000);

}
function draw_itemrepeat(){//item再一次
    // background("#feeacc")
    // if (videoCapture) {
    //     // translate(videoCapture.width, 0);
    //     // scale(-1, 1);
    //     // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();

    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('再一次', 320, 240)
        .textAlign(CENTER);
    }
    let jsdata=jsmotion.items[motionindex].itemdata;
    // let _ind=frameindex%jsdata.info.length;
    // console.log('影片:',jsdata.demovideo);

    if(jsmotion.items[motionindex].autoflip==1){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(100)
        .stroke('white')
        .text('請換邊', 320, 380)
        .textAlign(CENTER);
        
    

    }else{

    }
    schdure(draw_nextstart,3000);
    // setTimeout(()=>{
    //     draw = draw_nextstart;
    // },3000);

}
function draw_finish(){//關卡完成
    // background("#feeacc")
    // if (videoCapture) {
    //     push();
    //     scale(-1, 1)
    //     image(videoCapture, -videoCapture.width, 0);
    //     pop();
    // } 
    if(true){
        fill('#AA660055')
        .strokeWeight(3)
        .textSize(140)
        .stroke('white')
        .text('結束', 320, 240)
        .textAlign(CENTER);
    }

}
function draw_playgo() {//遊戲進行
    // background("#feeacc")
    if (videoCapture) {
        // push();
        // scale(-1, 1)
        // image(videoCapture, -videoCapture.width, 0);
        // pop();
        // image(videoCapture, 0, 0, videoCapture.width, videoCapture.heigth);
        let jsdata;
        let ll;
        if(true){
            if (!poses) {
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(70)
                .stroke('white');
                text('人物請回到畫面中間', 320, 240).textAlign(CENTER);
                return;
            }
            let pose = poses[0]
            if (!pose) {
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(70)
                .stroke('white');
                text('人物請回到畫面中間', 320, 240).textAlign(CENTER);
                return;
            }
            jsdata=jsmotion.items[motionindex].itemdata;
            ppkey=jsdata.frame[frameindex%jsdata.frame.length];
            // for(let i=0;i<30;i++){
            // nowflip=jsmotion.items[motionindex].flip;
            // if(jsmotion.items[motionindex].autoflip==1){
            //     nowflip=(jsmotion.items[motionindex].flip+itemrepeat)%2;
            // }
            // console.log('嘿嘿嘿',nowflip);
            ll=FitScoreXObj.getScore(ppkey/**原始資訊 */,pose/**擷取資訊 */,flipstatus()/**左右互換 */);
            
            // if(ll==0){
            //     return;
            // }
            // ll=0.8
                // ll=FitScoreX.getScore(ppkey/**原始資訊 */,pose/**擷取資訊 */);
            // }
            // FitScore.getScore(ppkey/**目前輪到的動作對象 */,pose/**攝影機抓到的姿態 */)
            //拿取目前比對分數
            if(true){
                //呈現目前比對成績0~99
                fill('#AA660055')
                .strokeWeight(10)
                .textSize(240)
                .stroke('white');
                text(Math.floor(ll*100), 320, 240).textAlign(CENTER);
            }
            let _ind=frameindex%jsdata.info.length;
            let stayandholdtime=jsdata.info[_ind].stay;
            
            jsdata.info[_ind].demovideo;


            
            if(jsdata.info[_ind].ishold==1){
                stayandholdtime=jsmotion.items[motionindex].holdtime
            }
            if(ll>jsdata.info[_ind].accuracy_level){//得到正確的精度
                
                fittime++
                // draw_process(fittime/30,jsdata.info[_ind].stay);
                
                draw_process(fittime/20,stayandholdtime);
                //console.log('9888=',jsdata.info[_ind].ishold,stayandholdtime,jsmotion.items[motionindex].holdtime)
                // if((fittime/30)>=jsdata.info[_ind].stay){
                if((fittime/20)>=stayandholdtime){
                    if(frameindex==0){
                        voicestart.play();                        
                    }else{
                        voiceary[_ind%4].play();
                    }
                    
                   
                    // if(jsdata.info[frameindex].ishold==1){
                    //如果這個姿勢是ishold，那麼就必須從設定支撐時間來計算～直到充飽時間才行到下個動作
                    //尚未寫
                    fittime=0;
                    frameindex++;
                    cunt+=jsdata.info[_ind].count;
                    if(frameindex%jsdata.info.length==0){//連續動作做完了一次
                        console.log('單一動作完成'+cunt+'次');
                        if(cunt>=jsmotion.items[motionindex].count){
                            console.log('單一動作完成足夠的數量'+cunt);
                            itemrepeat++;
                            cunt=0;
                            if(itemrepeat>=jsmotion.items[motionindex].repeat){
                                console.log('滿足單一次數與單一重複次數，前進下一關');
                                motionindex++;
                                itemrepeat=0;
                                if(motionindex>=jsmotion.items.length){
                                    console.log('單一活動結束')
                                    motionrepeat++;
                                    motionindex=0;
                                    if(motionrepeat>=jsmotion.repeat){
                                        console.log('全部活動結束')
                                        motionrepeat=0;
                                        draw2=draw_finish;
                                    }else{
                                        console.log('進入第'+motionrepeat+'輪完整課表');
                                    }
                                }else{
                                    //下一關
                                    draw2=draw_clear;
                                    
                                }
                            }else{
                                draw2=draw_itemrepeat;
                                console.log('單一動作進行重複第'+itemrepeat+'輪');
                            }
                        }
                    }
                }
            }else{
                fittime=Math.max(fittime-1,0);
                // draw_process(fittime/30,jsdata.info[_ind].stay);
                draw_process(fittime/20,stayandholdtime);
            }
            fill('#AA0000')
            .strokeWeight(4)
            .textSize(100)
            .stroke('white');    
            text(cunt, 80, 440).textAlign(LEFT);
            function draw_process(val,tagval){//呈現累積畫面
                if(tagval==0){
                    return;
                }
                let gg=val/tagval;
                push()
                noStroke()
                fill('#AA6600')
                // .strokeWeight(4)
                // .textSize(40)
                // .stroke('white');    
                // text(Math.floor(gg*100)+'%', 320, 440).textAlign(CENTER);    
                // rect(0,windowHeight-100,gg/windowWidth,windowHeight);
                // rect(0,windowHeight-100,gg/windowWidth,windowHeight);
                // console.log(640*gg);
                rect(0,apos.height-100,windowWidth*gg,apos.height);
                pop()
            }

        }
        drawKeypoints(poses);
        drawSkeleton(poses); 
        drawDemovideo();
        // }
        return;

    }

    // drawGui();
    fill('#AA6600')
        .strokeWeight(4)
        .textSize(40)
        .stroke('white');

    text('啟動中', 200, 200).textAlign(CENTER);
}
function drawKeypoints(poses) {//畫肢體點
    let _nowflip=flipstatus()
    if (!poses) {
        return;
    }
    // console.log(poses)
    let pose = poses[0]

    if (!pose) {
        return;
    }
    for (let j = 0; j < pose.keypoints.length; j++) {
        let jj=j
        if(_nowflip==1){
            jj=ptcov[j];
        }
        // console.log(nowflip,'嗯嗯嗯嗯',jj,j)
        let keypoint = pose.keypoints[jj];
        // console.log(keypoint,'pt'+j,ppkey['pt'+j])
        if(!ppkey['pt'+j]){
            continue;
        }else{
            if(ppkey['pt'+j].uselv<-1){
                continue;
            }
        }
        if (keypoint.score > 0.2) {
            fill(100, 255, 0);
            stroke(255, 188, 0);
            ellipse((videoCapture.width-keypoint.x)*captureratio+offsetx, keypoint.y*captureratio+offsety, 10, 10);
        }
    }
}
function drawSkeleton(poses) {//畫肢體線
    let _nowflip=flipstatus()

    if (!poses) {
        return;
    }
    if (!poses[0]) {
        return;
    }
    let pose = poses[0].keypoints
    for (let j = 0; j < pointAry.length; j++) {
        let _a=pointAry[j][0]
        let _b=pointAry[j][1]
        // let jj=j
        if(_nowflip==1){
            _a=ptcov[_a];
            _b=ptcov[_b];
        }

        if(!ppkey['pt'+pointAry[j][0]]){
            continue;
        }
        if(!ppkey['pt'+pointAry[j][1]]){
            continue;
        }

        // else{
        if(ppkey['pt'+pointAry[j][0]].uselv<-1){
            continue;
        }
        if(ppkey['pt'+pointAry[j][1]].uselv<-1){
            continue;
        }
        // }
        let a = pose[_a];
        let b = pose[_b];
        if (a.score < 0.2 || b.score < 0.2) {
            continue;
        }
        strokeWeight(4);
        stroke(255, 180, 0);
        //鏡射的關係，所以要反過來畫
        line(videoCapture.width*captureratio-a.x*captureratio+offsetx, a.y*captureratio+offsety, videoCapture.width*captureratio-b.x*captureratio+offsetx, b.y*captureratio+offsety);
    }
}



function windowResized() {//視窗縮放
    resizeCanvas(windowWidth, windowHeight);
}
