

```flow
  flowchat
  st=>start: 拿到腳本
  op=>operation: Your Operation
  op2=>operation: Your Operation2
  cond=>condition: Yes or No?
  e=>end
  st->op->cond
  cond(yes)->e
  cond(no)->op
```


# 國模數理科製作流程
## 不論是html or swf ，都必須等swf美術完成

#國模數理科製作流程分析

包明勝
- ## 環境需求
 ### VSCode 與常見快速鍵技巧
### Adobe Flash CS6
### Chrome
### Excel
- ## 具備技術能力
### XML理解能力
### Json理解能力
### Flash架構理解能力
### HTML基礎
### CSS基礎
- ## 工作內容
### SWF 1.0版課程開發
### SWF 2.0版課程開發
### HTML5課程開發
### SWF實驗元件
### HTML實驗元件
### SWF考題製作
### HTML考題製作
- ## 產出流程圖
https://maxpao.gitlab.io/mywebproject


```mermaid
graph TD;
    拿到新課程腳本-->確定課程版本需求-->製作初始XML;
    製作初始XML-->按照腳本需求調整分鏡;
    按照腳本需求調整分鏡-->輸出XML
    拿到Q單-->判斷需不需要調整分鏡
    判斷需不需要調整分鏡-->需要調整分鏡-->按照腳本需求調整分鏡
    判斷需不需要調整分鏡-->不需要調整分鏡-->等候美術完成
    輸出XML-->使用產生器去吃XML
    使用產生器去吃XML-->產生器調整細項需求
    產生器調整細項需求-->產出母包
    產出母包-->等候美術完成
    等候美術完成
    等候美術完成-->如果這次做的是Html
    如果這次做的是Html-->使用阿傑的截圖工具抓取美術給的swf檔案
    使用阿傑的截圖工具抓取美術給的swf檔案-->產出成圖像檔案
    等候美術完成-->如果這次做的是SWF
    產出成圖像檔案-->利用產生器設定css及各種座標大小-->利用產生器跑批次-->產出html課程包-->課程包放置在測試位置
    如果這次做的是SWF-->把美術檔案放置入swf課程包
    把美術檔案放置入swf課程包-->開啟tm.swf測試
    課程包放置在測試位置-->開啟伺服器127.0.0.1用chrome測試
    開啟伺服器127.0.0.1用chrome測試-->html看到問題-->回報swf美術
    開啟伺服器127.0.0.1用chrome測試-->html沒有問題-->上傳課程
    開啟tm.swf測試-->swf看到問題-->回報swf美術-->等候美術完成
    開啟tm.swf測試-->swf沒有問題-->上傳課程-->完成

    style 拿到Q單 fill:#00ffff
    style 拿到新課程腳本 fill:#00ffff
    style 等候美術完成 fill:#f9f
    style 完成 fill:#00ff00

```
